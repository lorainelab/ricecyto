#!/bin/bash


# rice 2bit file (need absolute path)
# get it from http://www.igbquicklaod.org/quickload
G=$HOME/src/genomes/pub/quickload/O_sativa_japonica_Jun_2009/O_sativa_japonica_Jun_2009.2bit

# BED file with promoters
P=Promoters1000.bed

# note: this doesn't do exactly what we want. It emits sequence
# but the fasta headers don't include the gene name
python -m twobitreader $G < $P


