#!/usr/bin/env python

# There were 108 regions associated with multiple gene ids.


import argparse,sys,re,twobitreader

ex=\
"""
Make motif counts file.
"""

"""
Output:
    gene - MSU7 identifier for gene associated with the region
    start - region start, interbase coordinates
    end - region end, interbase coordinates
    strand - strand of gene, values are + or -
    rank - binding site rank in OsRR22_results.txt
    sequence - binding site sequence (plus strand)
    score - binding site E-score
    num.instances - number of times the motif occurs in the region
"""

def getSeqDict(twobit_file=None):
    genome=twobitreader.TwoBitFile(twobit_file)
    return genome

def getSequence(genome,seqname,start,end):
    return(genome[seqname][start:end])

def countSites(regex,sequence):
    pass

def read_sites_file(sites_file=None):
    lines=open(sites_file).read().split('\n')
    probes=[]
    i=0
    for line in lines[1:]:
        if line == '':
            continue
        i=i+1
        (forward,reverse,score)=line.split()
        probes.append([forward,reverse,score,i])
    return probes

def find_site(promoter,sequence,starts={}):
    i = promoter.find(sequence)
    while i >= 0:
        starts[i]=i
        i=promoter.find(sequence,i+1)
        #sys.stderr.write("i is %i\n"%i)
    return starts

def main(twobit_file=None,
         promoter_file=None,
         sites_file=None):
    sites=read_sites_file(sites_file[0])
    genome=getSeqDict(twobit_file=twobit_file[0])
    fh = open(promoter_file[0])
    heads=['gene','start','end','strand','rank','sequence',
           'score','num.instances']
    sys.stdout.write('\t'.join(heads)+'\n')
    while 1:
        line = fh.readline()
        if not line:
            break
        (seqname,start,end,name,score,strand)=line.rstrip().split('\t')
        promoter=getSequence(genome,seqname,int(start),int(end))
        for site in sites:
            starts={}
            vals=[name,str(start),str(end),strand]
            forward=site[0]
            reverse=site[1]
            score=site[2]
            rank=str(site[3])
            find_site(promoter,forward,starts) # find forward
            find_site(promoter,reverse,starts) # find reverse
            # note: we don't want to count palindromes twice so
            # use a hashtable to avoid repeats
            N = len(starts.keys())
            if N>0:
                to_write=vals+[rank,forward,score,str(len(starts.keys()))]
                sys.stdout.write('\t'.join(to_write)+'\n')
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=ex)
    parser.add_argument("-t","--twobit_file",help='twobit file for genome (required)',
                        dest="twobit_file",nargs=1)
    parser.add_argument("-p","--promoter_file",
                        help="name of bed file with promoter regions (required)",
                        dest='promoter_file',nargs=1)
    parser.add_argument("-s","--sites_file",
                        help="name of the file containing sites to search for (required)",
                        dest="sites_file",nargs=1)
    args=parser.parse_args()
    main(twobit_file=args.twobit_file,
         promoter_file=args.promoter_file,
         sites_file=args.sites_file)

