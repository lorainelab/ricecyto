#!/bin/bash

# make a gene regions 4-column tabular file using bedtools
# column 4 contains names of merged gene models, column 5 is their strand
# only models on the same strand get merged
# see:
# http://bedtools.readthedocs.org/en/latest/content/tools/merge.html

# RGAP gene models (MSU7) in bed detail format
G=O_sativa_japonica_Oct_2011
F=$G.bed.gz

if [ ! -e $F ] 
then
    wget http://www.igbquickload.org/quickload/$G/$F 
else
    echo "No need to get $F." 1>&2
fi

M=Merged.bed
echo "Making merged bed file $M." 1>&2
# old bedtools, new versions better!
# gunzip -c $F | cut -f1-12 | mergeBed -nms -s > $M 
gunzip -c $F | cut -f1-12 > $G.bed
bedtools merge -i $G.bed -s -c 4,6 -o collapse,distinct > $M
rm $G.bed
