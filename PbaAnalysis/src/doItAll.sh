#!/bin/bash

# example showing how to run the pipeline
# Rice genome 
G=O_sativa_japonica_Oct_2011

get2Bit.sh # gets 2bit if not already here
mergeModels.sh # runs bedtools, makes Merged.bed
makePromoterBed.py -s 1000 Merged.bed > Promoter.bed # gets promoter regions size 1000
countSequences.py -p Promoter.bed -s ../data/OsRR22_results.txt -t $G.2bit > promsites.txt 
