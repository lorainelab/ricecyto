#!/bin/bash

# make a gene regions 4-column tabular file using bedtools
# column 4 contains names of merged gene models, column 5 is their strand
# only models on the same strand get merged
# see:

# Rice genome 
G=O_sativa_japonica_Oct_2011
F=$G.2bit

if [ ! -e $F ] 
then
    wget http://www.igbquickload.org/quickload/$G/$F 
else
    echo "No need to get $F." 1>&2
fi
