# Code that investigates prevalence of OsRR22 binding sites in promoter regions of rice genes

## What's here

* src - code that counts binding site instances in genes
* Enrichment - tests for enrichment using random sampling, makes figure
* NumSites - tabulates site instances
* data - results from protein binding array (PBA)
* doc - PowerPoint presentation describing PBA results
