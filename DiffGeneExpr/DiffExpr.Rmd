Effects of cytokinin on gene expression in rice seedings
============================================================

Introduction
------------

```{r echo=FALSE}
library(knitr)
#knitr::opts_chunk$set(fig.path = 'figure/')
knit_hooks$set(inline = function(x) {
  prettyNum(x, big.mark=",", digits=2)
})
```

Rice seedlings growing hydroponically in a growth chamber were placed in media containing either 5 micromolar BA (a cytokinin) or a mock treatment with BA solvent (NaOH) for 2 hours. Roots and shoots were collected separately and used to prepare libraries for 100 bp Illumina sequencing. Sequencing was done at the DHMRI at the NCRC in Kannapolis. Libraries were made by Dr. Yu-Chang Tsai, postdoctoral fellow in the Kieber Lab.

### Expectations

Based on prior results from rice and other plants, we expect:

* There will be more DE genes in the roots than shoots because roots were directly exposed to cytokinin whereas cytokinin reaching the shoots had to be transported.
* Genes responsible for the different physiological responses of roots and shoots to cytokinin will be differentially expressed in either roots or shoots, but not in the same way.

Questions we aim to answer:

* How many genes were DE in roots and shoots?
* How many genes differed with respect to their responsiveness to BA in roots and shoots? 
* Were fold-changes larger in roots?

Analysis
--------

Load library and project-wide variables:

```{r}
suppressPackageStartupMessages(library(edgeR))
source('../src/rice_functions.R')
de_FDR=getFDR()
```

To start, we picked an FDR cutoff of `r de_FDR` for differential gene expression.

Read gene counts and annotations:

```{r}
fname='../Counts/results/rice.tsv.gz'
d=getCounts(fname)
fname='../ExternalDataSets/O_sativa_japonica_Oct_2011.bed.gz'
annots=getAnnots(fname)
fname='../Counts/results/rice_RPKM.tsv.gz'
RPKM=getRPKM(fname)
CR=grep('CR',names(d))
CS=grep('CS',names(d))
TR=grep('TR',names(d))
TS=grep('TS',names(d))
cols=c(CR,CS,TR,TS)
d=d[,cols]
dge=DGEList(d[,cols],remove.zeros=T)
dge=calcNormFactors(dge)
```

Out of `r nrow(d)` we detected at least one read for `r nrow(dge$counts)` genes, or `r round(nrow(dge$counts)/nrow(d)*100,1)`% of2890 genes in the rice genome. 

### Sample clustering

#### Multi-dimensional scaling

```{r fig.width=6,fig.height=6}
colnames=colnames(dge$counts)
colors=getColors(colnames)
main='MDS plot'
plotMDS(dge,col=colors,main=main)
f='results/MDS.tiff'
quartz.save(file=f,type='tiff',dpi=300,height=4,width=4)
```

Results:

* Dim 1 separates roots from shoots.
* Dim 2 separates control roots from treatment roots, indicating there are many DE genes in treated roots versus untreated roots.
* Shoots samples overlap, indicating there are not as many DE genes in treated shoots versus untreated shoots.

#### Hierarchical clustering

```{r fig.height=4,fig.width=4}
normalized.counts=cpm(dge)
transposed=t(normalized.counts) # transposes the counts matrix
distance=dist(transposed) # calculates distance
clusters=hclust(distance) # does hierarchical clustering
makeClusterPlot(clusters,lab.col=colors)
fname='results/Hclust.tiff'
quartz.save(file=fname,type='tiff',dpi=300,height=4,width=4)
```

Results:

* Samples cluster by treatment.

### Differential Expression 

### BA vs mock treated root (R)

```{r fig.width=5,fig.height=5}
base='R'
res.R=testDE(Cn='CR',Tr='TR',counts=d,annots=annots,
             basename=base,RPKM=RPKM)
res=res.R
```

Made `r base`.tsv.gz in results.

There were:

* `r sum(res$fdr<=de_FDR)` differentially expressed genes
* `r sum(res$fdr<=de_FDR & res$logFC>0)` up regulated genes
* `r sum(res$fdr<=de_FDR & res$logFC<0)` down regulated genes

at FDR `r de_FDR`.

```{r}
fname = saveDEgenes(res=res, fdr=de_FDR, base=base)
```
`r fname` contains only the DE genes, and only gene name and logFC columns.

### BA vs mock treated shoot (S)

```{r fig.width=5,fig.height=5}
base='S'
res.S=testDE(Cn='CS',Tr='TS',counts=d,annots,
             basename=base,RPKM=RPKM)
res=res.S
```

Made `r base`.tsv.gz in results.

There were:

* `r sum(res$fdr<=de_FDR)` differentially expressed genes
* `r sum(res$fdr<=de_FDR & res$logFC>0)` up regulated genes
* `r sum(res$fdr<=de_FDR & res$logFC<0)` down regulated genes

at FDR `r de_FDR`.

```{r}
fname = saveDEgenes(res, de_FDR, base)
```
`r fname` contains only the DE genes, and only gene name and logFC columns.

### Root vs shoot, controls only

```{r fig.width=5,fig.height=5}
base='C'
res.C=testDE(Cn='CR',Tr='CS',counts=d,annots=annots,
              basename=base,RPKM=RPKM)
res=res.C
```

Made `r base`.tsv.gz in results.

There were:

* `r sum(res$fdr<=de_FDR)` differentially expressed genes
* `r sum(res$fdr<=de_FDR & res$logFC>0)` up regulated genes
* `r sum(res$fdr<=de_FDR & res$logFC<0)` down regulated genes

at FDR `r de_FDR`.

Testing interaction between treatment and tissue type
-----------------------------------------------------

Find out which, if any, genes respond differently to BA depending on site of expression:

```{r}
cols=c(CR,CS,TR,TS)
dge=DGEList(counts=d[,cols],remove.zeros=T)
Treatment=factor(c(rep('C',6),rep('T',6)))
Tissue=factor(c(rep('R',3),rep('S',3),rep('R',3),rep('S',3)))
design=model.matrix(~Treatment*Tissue)
rownames(design)=rownames(dge$samples)
dge = calcNormFactors(dge)
dge = estimateGLMCommonDisp(dge,design)
dge = estimateGLMTrendedDisp(dge,design)
dge = estimateGLMTagwiseDisp(dge,design)
fit = glmFit(dge,design)
lrt.int = glmLRT(fit, coef=4)
lrt.int$table$fdr = p.adjust(lrt.int$table$PValue)
```

Write results:

```{r}
fname = "results/I.tsv"
res.I=data.frame(RPKM[row.names(lrt.int$table),c('CR','TR','CS','TS')],
                 fdr=lrt.int$table$fdr,
                 gene=row.names(lrt.int$table))
res.I=merge(res.I,annots,by.x='gene',by.y='gene')
o=order(res.I$fdr)
res.I=res.I[o,]
write.table(res.I, file = fname, row.names = F, 
            sep = "\t", quote = F)
system(paste("gzip -f", fname))
````

Made `r fname`.gz.

At `r de_FDR` FDR there were `r sum(res.I$fdr<=de_FDR)` genes that responded differently to cytokinin depending on site of expression. (Fold change is not directly relevant for this test.)

This result is hard to interpret because for some genes, the difference in cytokinin responsivess may have
been due to different concentrations of cytokinin in roots versus shoots. We don't know how much cytokinin
reached the shoots. To investigate, later we'll examine  fold-change and expression of known cytokinin response genes in the treated roots and shoots.

### Summary

*Table of DE genes (fdr <= `r de_FDR`)*

Comparison  | Number DE genes
------------- | -------------
Roots Untreated vs. Treated  | `r sum(res.R$fdr<=de_FDR)`
Shoots Untreated vs. Treated  | `r sum(res.S$fdr<=de_FDR)`
Roots vs. Shoots Untreated  | `r sum(res.C$fdr<=de_FDR)`
Interaction  | `r sum(res.I$fdr<=de_FDR)`

Comparing fold-changes in roots versus shoots
---------------------------------------------

There were more DE genes in roots than in shoots, maybe because the roots were exposed to higher concentration of BA.

If yes, then magnitude of the differences should be larger in roots than in shoots.

Test this:

```{r}
r=res.R[res.R$fdr<=de_FDR,]
row.names(r)=r$gene
s=res.S[res.S$fdr<=de_FDR,]
row.names(s)=s$gene
genes=intersect(r$gene,s$gene)
both=merge(r[genes,],s[genes,],by.x='gene',by.y='gene')
both$bothDE = both$fdr.x < de_FDR & both$fdr.y < de_FDR
both=both[both$bothDE,]
both=both[,c('gene','descr.x','logFC.x','logFC.y')]
names(both)=c('gene','descr','logFC.root','logFC.shoot')
# highlight the type As
typeAs = getTypeARRs()
ta = which(both$gene %in% typeAs)
```

Plot log2FC shoot versus root:

```{r fig.width=5, fig.height=5}
par(las=1, mai=c(1,1,1,1))
sqr=5 #for a square shaped plot
main=expression(paste("log"[2],"FC response to treatment"))
ylab=expression(paste("roots log"[2],"FC"))
xlab=expression(paste("shoots log"[2],"FC"))

plot(both$logFC.root~both$logFC.shoot,pch=20, xaxt="n", yaxt="n",
     main="",xlab="",ylab="", xlim=c(-sqr,sqr), ylim=c(-sqr,sqr))
mtext(text=main, side=3, cex=1.3, line=1)
# add type-A RRs in red
points(both$logFC.root[ta]~both$logFC.shoot[ta],pch=20, col="blue")
# x-axis
axis(side=1, at=c(-4:4), labels=F, tick=T, line=0)
axis(side=1, at=c(-4:4), labels=T, tick=F, line=-.3)
mtext(text=xlab, side=1, cex=1, line=2.2)
# y-axis
axis(side=2, at=c(-4:4), labels=F, tick=T, line=0)
axis(side=2, at=c(-4:4), labels=T, tick=F, line=-.3)
mtext(text=ylab, side=2, cex=1, line=1.8, las=0)
# add lines
abline(a = 0, b = 1, col="red")
abline(a = 0, b = -1, col="red")
abline(h=0) #col=getColors("CS")
abline(v=0)
fname='results/FC.tiff'
quartz.save(fname,type='tiff',dpi=600,width=5,height=5)
```

Type-A RRs are indicated in blue.
Red lines indicate unity.

```{r echo=FALSE}
sameway=both$logFC.root * both$logFC.shoot > 0 #both have positive FC or both negative FC.
```

* `r nrow(both)` genes were DE in roots and shoots, at FDR `r de_FDR`.
* `r sum(sameway)` DE genes (`r round(sum(100*sameway)/nrow(both),1)`%) changed in the same direction in both root and shoot.

```{r}
sameway=both[sameway,]
```

* For most DE genes, the direction of the change is the same in roots and shoots.
* Typically, the magnitude of the change is greater in roots.

Conclusions
-----------

* As expected, there were many more DE genes in the roots treated versus untreated comparison.
* There were also a large number of genes that responded differently to the treatment depending on whether they were in roots or shoots.
* A very large number of genes were DE between shoot and roots, reflecting the huge differences between these two sample types. 

Session Info
------------

```{r}
sessionInfo()
testDE
```
