Genes DE in both roots and shoots that changed in the same direction
========================================================

Introduction
------------

Cytokinin affects different parts of plant bodies in different ways. In Arabidopsis, cytokinin inhibits root growth and stimulates formation of root hairs. In shoots, it stimulates growth, promotes greening, and retards senescence. And yet, the same two-component system gene families carry out cytokinin signaling everywhere. 

This Markdown aims to determine what other factors may play a role in the core cytokinin response by examining genes that are induced in both roots and shoots.

Questions:

* What genes changed in both sites, and in the same direction?
* How many genes were changed in the same direction but changed more in shoots?

Higher concentrations of cytokinin have larger effects. Because the treatment was applied via roots, we expect that fold-change for genes that are DE in both sites is generally larger for roots. 

Analysis
--------

Read DE results:

```{r}
fname='results/R.tsv.gz'
roots=read.delim(fname,sep='\t',header=T,as.is=T,quote='')
row.names(roots)=roots$gene
fname='results/S.tsv.gz'
shoots=read.delim(fname,sep='\t',header=T,as.is=T,quote='')
row.names(shoots)=shoots$gene
source('../src/rice_functions.R')
de_FDR=getFDR()
roots=roots[roots$fdr<=de_FDR,]
shoots=shoots[shoots$fdr<=de_FDR,]
both=intersect(roots$gene,shoots$gene)
de_both=length(both)
```

There were `r de_both` genes that were DE in both roots and shoots.

```{r}
samedirection=(roots[both,]$logFC*shoots[both,]$logFC)>0
de_same_direction=sum(samedirection)
both=both[samedirection]
roots=roots[both,c('gene','logFC','CR','TR')]
names(roots)[2]='r.logFC'
shoots=shoots[both,c('gene','logFC','CS','TS','descr')]
names(shoots)[2]='s.logFC'
both=merge(roots,shoots,by.x='gene',by.y='gene')
```

There were `r de_both` genes that were DE in both roots and shoots and which changed in the same direction.

```{r}
higherInShoots=which(abs(both$s.logFC)>abs(both$r.logFC))
de_shoots_higher=length(higherInShoots)
```

There were `r de_shoots_higher` DE genes where the magnitude of the difference was larger in shoots than roots. This was `r round(de_shoots_higher/de_same_direction*100,1)`% of genes that changed in the same direction. 

How many genes that changed in both sites and in the same direction were transcription factors?

Retrieve a list of transcription factors from a Plant transcription factor database: 

```{r}
url='http://planttfdb.cbi.pku.edu.cn/download/gene_model_family/Osj'
tx=read.delim(url,header=T,sep='\t',as.is=T)
tx$gene=unlist(lapply(strsplit(tx$gene_model,'\\.'),
                      function(x)x[[1]]))
syngenta_genes=grep('Sy',tx$gene_model)
tx$gene[syngenta_genes]=tx$gene_model[syngenta_genes]
tx=tx[!duplicated(tx$gene),]
num_tx_in_genome=nrow(tx)
both=merge(both,tx[,c('gene','family')],
            by.x='gene',by.y='gene',all.x=TRUE)
num_tx=sum(!is.na(both$family))
```

There were `r num_tx_in_genome` genes annotated as transcription factors in the rice genome. 

Out of the `r nrow(both)` genes that were differentially expressed in both roots and shoots with larger fold-change in shoots, `r num_tx` were annotated as transcription factors. These included:

```{r}
both_tx=both[!is.na(both$family),]
both_tx[,c('gene','family','descr')]
```


See also:

* ../ExternalDataSets/RiceCytoGenes.xlsx, adapted from http://www.ncbi.nlm.nih.gov/pubmed/22383541
* http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3489639/ (review)

How many of the genes that were DE in both sites and in the same direction were known members of the core cytokinin signaling pathway? 

Read list of cytokinin genes:

```{r}
tcs=getTcsGenes()
tcs$tcs=paste(tcs$symbol,tcs$type)
both=merge(both,tcs[,c('gene','tcs')],by.x='gene',by.y='gene',all.x=TRUE)
```

Write results:

```{r}
fname='results/DESameDirection.txt'
write.table(both,fname,quote=F,sep='\t',row.names=F)
```

To find out more, open file `r fname`.

Conclusions
-----------

Answers:

* `r de_both` genes were DE in roots and shoots.
* `r de_same_direction` genes were DE in roots and shoots and changed in the same direction
* Of these, `r num_tx` were transcription factors.
* `r de_shoots_higher` genes were DE in both but with higher fold-change in shoots

Session information
-------------------

```{r}
sessionInfo()
```

