#!/usr/bin/env python

ex=\
"""

Extract GO terms for MSU gene annotations from Gramene GO file.
This only works because MSU gene ids follow a pattern. 
Reads from stdin, writes to stdout.

ex)

gunzip -c gene_association.gramene_oryza.gz | %prog > go2gene.tsv

MSU7 ids start with LOC_Os or ChrSy.fgenesh.mRNA

As of 12/14, there are not ChrSy genes in the GO annotation file.

Some GO/gene mappings may appear more than once in the output.
"""

import os,sys,optparse,fileinput,re

def main(args):
    sys.stdout.write('category\tGene\n')
    reg1=re.compile(r'LOC_Os..g\d{5}')
    # reg2=re.compile(r'ChrSy.fgenesh.mRNA.\d+')
    for line in fileinput.input(args):
        genes=reg1.findall(line)
        if len(genes)>0:
            toks=line.rstrip().split('\t')
            GO=toks[4]
            for gene in genes:
                sys.stdout.write('%s\t%s\n'%(GO,gene))
    # done

if __name__ == '__main__':
    usage = "%prog"+ex
    parser = optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args)

# this was useful
# http://stackoverflow.com/questions/9756396/remove-parsed-options-and-their-values-from-sys-argv
