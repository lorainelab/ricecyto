#!/usr/bin/env python

"""Create a data file we can use for mapping between RAP-DB and MSU7 gene names."""

ex=\
"""

Read mapping file downloaded from RAP-DB:

http://rapdb.dna.affrc.go.jp/download/archive/RAP-MSU.txt.gz

Create two-column table with MSU7 identifiers in column one
and RAP-DB identifiers in column 2.

ex)

gunzip -c RAP-MSU.txt.gz | %prog > msu2rap.tsv

MSU7 ids start with LOC_Os or ChrSy.fgenesh.mRNA
RAP-DB ids start with Os

For simplicity, only report mappings that are one to one, e.g., one
RAP-DB id maps onto one MSU7 id.
"""

import sys,optparse,fileinput

def main(args):
    msu2rap={}
    rapdb={}
    zeros={}
    multis={}
    for line in fileinput.input(args):
        toks=line.rstrip().split('\t')
        rap=toks[0]
        if len(toks)==1:
            zeros[rap]=0
        else:
            msus=toks[1].split(', ')
            if msus[0].startswith('ChrSy'):
                sys.stderr.write("Got: %s\n"%msus[0])
            subd={}
            for msu in msus:
                locus=msu.split('.')[0]
                subd[locus]=locus
            if len(subd.keys())>1:
                #sys.stderr.write("%s had multiple msu genes\n"%rap)
                multis[rap]=rap
            else:
                rapdb[rap]=subd.keys()[0]
    sys.stderr.write("%i RAP-DB ids had no MSU7 counterpart.\n"%len(zeros.keys()))
    sys.stderr.write("%i RAP-DB ids has >1 MSU7 locus.\n"%len(multis.keys()))
    sys.stdout.write('msu7\trap\n')
    counter=0
    for rap in rapdb.keys():
        msu=rapdb[rap]
        sys.stdout.write('%s\t%s\n'%(msu,rap))
        counter=counter+1
    sys.stderr.write("%i MSU7 loci had one and only one RAP-DB id.\n"%counter)

if __name__ == '__main__':
    usage = "%prog"+ex
    parser = optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args)

# this was useful
# http://stackoverflow.com/questions/9756396/remove-parsed-options-and-their-values-from-sys-argv

    
