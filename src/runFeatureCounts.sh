#!/bin/bash
SAF=$HOME/src/ricecyto/GeneRegions/results/SAF_for_featureCount.tsv
#BASE=roots2_counts
BASE=roots_shoots1_counts
OUT=$BASE.tsv
ERR=$BASE.err
#OUT2=$BASE.out
# strand-specific? if yes, use: -s <int> (2 for reversely stranded)
featureCounts -F SAF -a $SAF -o $OUT *.bam 2>$ERR 


