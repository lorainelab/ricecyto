#!/usr/bin/env python

# to run this, get git repository https://bitbucket.org/lorainelab/igbquickload
# put igbquickload root directory in your PYTHONPATH

import os
from Quickload import *
import QuickloadUtils as utils
from AnnotsXmlForRNASeq import *
import AnnotsXmlForSmMmRNASeq as sm_mm

ANNOTSBACKGROUND='FFFFFF'

# color for control, root
cr="1F78B4"
# color for control, shoot
cs="33A02C"
# color for treatment, root
tr='9900FF' 
# color for treatment, shoot
ts='8B2323'

u = 'O_sativa_japonica_Oct_2011/JK-TH2.0.5_processed'


samples =[['BA2h-R1','BA Root1',tr,ANNOTSBACKGROUND,u],
         ['BA2h-R2','BA Root2',tr,ANNOTSBACKGROUND,u],
         ['BA2h-R3','BA Root3',tr,ANNOTSBACKGROUND,u],
         ['Control2h-R1','Cntrl Root1',cr,ANNOTSBACKGROUND,u],
         ['Control2h-R2','Cntrl Root2',cr,ANNOTSBACKGROUND,u],
         ['Control2h-R3','Cntrl Root3',cr,ANNOTSBACKGROUND,u],
         ['BA2h-S1','BA Shoot1',ts,ANNOTSBACKGROUND,u],
         ['BA2h-S2','BA Shoot2',ts,ANNOTSBACKGROUND,u],
         ['BA2h-S3','BA Shoot3',ts,ANNOTSBACKGROUND,u],
         ['Control2h-S1','Cntrl Shoot1',cs,ANNOTSBACKGROUND,u],
         ['Control2h-S2','Cntrl Shoot2',cs,ANNOTSBACKGROUND,u],
         ['Control2h-S3','Cntrl Shoot3',cs,ANNOTSBACKGROUND,u]]

u2 = 'O_sativa_japonica_Oct_2011/root_tophat2.1.1_processed'

samples2=[['C1','Cntrl Root1',cr,ANNOTSBACKGROUND,u2],
          ['C2','Cntrl Root2',cr,ANNOTSBACKGROUND,u2],
          ['C3','Cntrl Root3',cr,ANNOTSBACKGROUND,u2],
          ['T1','BA Root1',tr,ANNOTSBACKGROUND,u2],
          ['T1','BA Root2',tr,ANNOTSBACKGROUND,u2],
          ['T1','BA Root3',tr,ANNOTSBACKGROUND,u2]]
          
def makeQuickloadFiles(fname=None):
    quickload_files = sm_mm.makeQuickloadFilesForRNASeq(lsts=samples,
                                    folder='Roots, Shoots - 1',
                                    deploy_dir=u.split(os.sep)[-1],all=False)
    rnaseq_files = makeQuickloadFilesForRNASeq(lsts=samples2,
                                folder='Roots - 2',
                                deploy_dir=u2.split(os.sep)[-1])
    for rnaseq_file in rnaseq_files:
        quickload_files.append(rnaseq_file)
    return quickload_files

if __name__=='__main__':
    about="Make annots.xml text for rice cytokinin data sets."
    utils.checkForHelp(about)
    quickload_files=makeQuickloadFiles()
    main(quickload_files=quickload_files) # main comes from imports

