
# Returns project-wide FDR cutoff for deciding
# differential expression in both roots and shoots.
getFDR=function() {
  return(0.0001)
}

# Returns named vector containing largest transcript (spliced, in kb)
# per locus.
getTranscriptSizes=function(fname='../ExternalDataSets/tx_size.txt.gz') {
  d=read.table(fname,as.is=T,header=T)
  sizes=d$bp/1000
  names(sizes)=d$locus
  return(sizes)
}

# get data frame containing transcription factor annotations
getTranscriptionFactorAnnotations=function() {
  url='http://planttfdb.cbi.pku.edu.cn/download/gene_model_family/Osj'
  tx=read.delim(url,header=T,sep='\t',as.is=T)
  tx$gene=unlist(lapply(strsplit(tx$gene_model,'\\.'),
                        function(x)x[[1]]))
  syngenta_genes=grep('Sy',tx$gene_model)
  tx$gene[syngenta_genes]=tx$gene_model[syngenta_genes]
  tx=tx[!duplicated(tx$gene),]
  tx=tx[,c('gene','family')]
  return(tx)
}
# get differential expression results for BA vs mock-treated roots
getDeResultsRoots=function(){
  fname='../DiffGeneExpr/results/R.tsv.gz'
  d=read.table(fname,as.is=T,quote='',header=T,sep='\t')
  row.names(d)=d$gene
  return(d)
}

# get differential expression resutls for BA vs mock-treated shoots
getDeResultsShoots=function(){
  fname='../DiffGeneExpr/results/S.tsv.gz'
  d = read.table(fname,as.is=T,quote='',header=T,sep='\t')
  row.names(d)=d$gene
  return(d)
}

# read average RPKM per sample type
getRPKM = function(fname='../Counts/results/rice_RPKM.tsv.gz'){
  rpm=read.delim(fname,header=T,sep='\t',as.is=T)
  colnames=grep('ave',names(rpm),value=T)
  aves=rpm[,colnames]
  rownames(aves)=rpm$gene
  names(aves)=sub('\\.ave','',names(aves))
  return(aves)  
}

# get gene to gene descriptions
getAnnots=function(fname='../ExternalDataSets//O_sativa_japonica_Oct_2011.bed.gz'){
  d=read.delim(fname,as.is=T,header=F)
  d=d[,c(13,14)]
  names(d)=c('gene','descr')
  d=d[!duplicated(d$gene),]
  row.names(d)=d$gene
  return(d)
}

getCounts=function(fname='../Counts/results/rice.tsv.gz'){
  d=read.delim(fname,header=T,sep='\t',as.is=T)
  row.names(d)=d$gene
  d=d[,-which(names(d)=='gene')]
  return(d)  
}

# read counts from FeatureCounts
getCounts2=function(fname){
  counts=read.delim(fname,header=T,sep='\t', comment.char="#")
  rownames(counts)=counts$Geneid
  counts=counts[,grep("bam", names(counts))]
  # change column names to match standard sample names
  names(counts) = gsub(".bam", "", names(counts))
  names(counts) = gsub("BA2h.", "T", names(counts))
  names(counts) = gsub("Control2h.", "C", names(counts))
  return(counts)
}
  
  

# color for control, root
CR.color="#1F78B4"#RCBdarkerblue # '#336600'#darkgreen
# color for control, shoot
CS.color="#33A02C"#RCBdarkergreen # '#008B8B'#turqoise
# color for treatment, root
TR.color='#9900FF'#purple 
# color for treatment, shoot
TS.color='#8B2323'#red

getColors = function(v) {
  a=grep('CR',v)
  b=grep('TR',v)
  c=grep('CS',v)
  d=grep('TS',v)
  colors = 1:max(a,b,c,d)
  colors[a]=CR.color
  colors[b]=TR.color
  colors[c]=CS.color
  colors[d]=TS.color
  names(colors)=v
  return(colors)
}

makeClusterPlot=function(hc,lab=hc$labels,lab.col=rep(1,length(hc$labels)),
                         hang=0.1,axes=F, ...) {
  y = rep(hc$height,2)
  x = as.numeric(hc$merge)
  y = y[which(x<0)]
  x = x[which(x<0)]; x=abs(x)
  y = y[order(x)]; x = x[order(x)]
  plot(hc,labels=FALSE,hang=hang,axes=axes, ...)
  text(x=x, y=y[hc$order]-(max(hc$height)*hang),
       labels=lab[hc$order], col=lab.col[hc$order],
       srt=90, adj=c(1,0.5),xpd=NA, ... )
}

# Tr - treatment group, TR - treatment, root
# Cn - control group, CR - control, root
# d - counts data, entire data frame
# file - root name of results file to write
#        tsv file extension will be appended
# aves - output of getRPKM
# This function tests differential expression between
# treatment (Tr) and control (CR) groups, saves results
# to a tab-separated value (tsv) file, and makes an MDS
# plot. It saves the plot to a tiff file (600 dpi) in
# a "results" subdirectory, which should exists in the 
# context where this is run.
testDE = function(Tr='TR',Cn='CR',counts=NULL,basename='R',
                  RPKM=NULL,annots=NULL) {
  d=counts
  imagefile=paste0('results/MDS-',basename,'.tiff')
  fname=paste0('results/',basename,'.tsv')
  c.indexes=grep(Cn,names(d))
  t.indexes=grep(Tr,names(d))
  group=c(rep(Cn,length(c.indexes)),rep(Tr,length(t.indexes)))
  counts=d[,c(c.indexes,t.indexes)]
  rownames(counts)=rownames(d)
  dge=DGEList(counts,group=group,remove.zeros=T)
  dge=calcNormFactors(dge)
  main=paste("MDS Plot for",Cn,"versus",Tr)
  colnames=colnames(dge$counts)
  colors=getColors(colnames)
  plotMDS(dge,main=main,col=colors)
  quartz.save(file=imagefile,type='tiff',dpi=300,height=4,width=4)
  dge = estimateCommonDisp(dge)
  prior.df=50/(ncol(counts)-length(unique(group)))
  dge=estimateTagwiseDisp(dge,prior.df=prior.df)
  dex=exactTest(dge,dispersion="tagwise",pair=unique(group))
  dex$fdr=p.adjust(dex$table$PValue,method='BH') 
  res=data.frame(gene=rownames(dge$counts),
                logFC=dex$table$logFC,
                fdr=dex$fdr,
                RPKM[rownames(dge$counts),unique(group)])
  res=merge(res,annots,by.x='gene',by.y='gene')
  res=res[order(res$fdr),]
  write.table(res,file=fname,row.names=F,sep='\t',quote=F)
  system(paste('gzip -f',fname))
  return(res)
}

saveDEgenes = function(res, fdr, base){
  towrite = res[res$fdr<=fdr,]
  towrite = towrite[,c("gene", "logFC")]
  ord = order(towrite$logFC, decreasing=T)
  towrite = towrite[ord,]
  fname = paste0("results/", base, "-DEonly.txt")
  write.table(towrite,file=fname,row.names=F,sep='\t',quote=F)
  system(paste('gzip -f',fname))
  return(fname)
}

# Read TCS genes
getTcsGenes = function(fname='../ExternalDataSets/RiceCytoGenes.xlsx') {
  suppressPackageStartupMessages(library(xlsx))
  d=read.xlsx2(fname,sheetIndex=1)[,c(3,1,5)]
  names(d)=c('gene','symbol','type')
  d$gene=as.character(d$gene)
  d$symbol=as.character(d$symbol)
  d$type=as.character(d$type)
  return(d)
}

# differential expression in both roots and shoots.
getAtFDR=function() {
  return(0.05)
}

  
### Tsai et al. 2012 outlined sets of genes related to cytokinin.
### Use the following functions to get a vecoter of gene ids for each group
# Rice cytokinin receptors
getCytoRreceptorGenes = function(){
  ret=c("LOC_Os01g69920","LOC_Os03g50860","LOC_Os10g21810","LOC_Os02g50480","LOC_Os12g26940")
  names(ret)=c("OsHK3", "OsHK4", "OsHK5", "OsHK6", "OsCRL4")
  return(ret)
}
# Rice Histidine phosphotransfer protein gene family
getHistPhosphotransferGenes = function(){
  ret=c("LOC_Os08g44350", "LOC_Os09g39400", "LOC_Os01g54050", "LOC_Os05g09410", "LOC_Os05g44570")
  names(ret)=c("OsAHP1", "OsAHP2", "OsPHP1", "OsPHP2", "OsPHP3")
  return(ret)
}
# Rice type-A response regulators
getTypeARRs = function(){
  ret=c("LOC_Os04g36070", "LOC_Os02g35180", "LOC_Os02g58350", "LOC_Os01g72330", "LOC_Os04g44280", "LOC_Os04g57720", "LOC_Os07g26720", "LOC_Os08g28900", "LOC_Os11g04720", "LOC_Os12g04500", "LOC_Os02g42060", "LOC_Os08g28950", "LOC_Os08g26990")
  names(ret)=c("OsRR1", "OsRR2", "OsRR3", "OsRR4", "OsRR5", "OsRR6", "OsRR7", "OsRR8", "OsRR9", "OsRR10", "OsRR11", "OsRR12", "OsRR13")
  return(ret)
}
# Rice type-B response regulators
getTypeBRRs = function(){
  ret=c("LOC_Os03g12350", "LOC_Os06g08440", "LOC_Os02g55320", "LOC_Os02g08500", "LOC_Os06g43910", "LOC_Os01g67770", "LOC_Os05g328800", "LOC_Os04g28160", "LOC_Os04g28130", "LOC_Os10g32600", "LOC_Os08g35650","LOC_Os08g17760", "LOC_Os08g35670")
  names(ret)=c("OsRR21", "OsRR22", "OsRR23", "OsRR24", "OsRR25", "OsRR26", "OsRR27", "OsRR28", "OsRR29","OsRR30", "OsRR31", "OsRR32", "OsRR33")
  return(ret)
}
# Rice type-C response regulators
getTypeCRRs = function(){
  ret=c("LOC_Os03g53100", "LOC_Os04g13480")
  names(ret)=c("OsRR41", "OsRR42")
  return(ret)
}
# Rice pseudo-response regulators
getPseudoRRs = function(){
  ret=c("LOC_Os02g40510","LOC_Os07g49460", "LOC_Os03g17570", "LOC_Os11g05930", "LOC_Os09g36220", "LOC_Os05g32890", "LOC_Os04g28150", "LOC_Os04g28120")
  names(ret)=c("OsPRR1", "OsPRR37", "OsPRR73", "OsPRR59", "OsPRR95", "OsPRR10", "OsPRR11", "OsPRR12")
  return(ret)
}
# Rice LONELY GUY genes
getLOGgenes = function(){
  # This set was taken from Kuroha et al, 2009 "Functional Analyses of LONELY GUY Cytokinin-Activating Enzymes Reveal the Importance of the Direct Activation Pathway in Arabidopsis"
  # The ids were in RAP id format, so I used the rap-db converter to get the MSU7 ids.
  ret = c("LOC_Os01g40630", "LOC_Os01g51210", "LOC_Os02g41770", "LOC_Os03g01880", "LOC_Os03g49050", "LOC_Os03g64070", "LOC_Os04g43840", "LOC_Os05g46360", "LOC_Os05g51390", "LOC_Os09g37540", "LOC_Os10g33900")
  names(ret) = c("LOG", "LOGL1", "LOGL2", "LOGL3", "LOGL4", "LOGL5", "LOGL6", "LOGL7", "LOGL8", "LOGL9", "LOGL10")
  return(ret)
}
# Rice cytokinin dehydrogenases
# From Tracy's spreadsheet, also see 
# "Molecular, phylogenetic and comparative genomic analysis of the cytokinin oxidase⁄dehydrogenase gene family in the Poaceae"
getCKXs = function(){
  ret = c("LOC_Os01g09260","LOC_Os01g10110","LOC_Os10g34230","LOC_Os01g71310","LOC_Os01g56810","LOC_Os02g12770","LOC_Os02g12780","LOC_Os04g44230","LOC_Os05g31040","LOC_Os06g37500","LOC_Os08g35860")
  names(ret) = c("OsCKX1","OsCKX2","OsCKX3","OsCKX4","OsCKX5","OsCKX6","OsCKX7","OsCKX8","OsCKX9","OsCKX10","OsCKX11")
  return(ret)
}
# Rice IPT genes
# From Tracy's spreadsheet
getIPTs = function(){
  ret=c("LOC_Os03g24440","LOC_Os03g24240","LOC_Os05g24660","LOC_Os03g59570","LOC_Os07g11050","LOC_Os07g09220","LOC_Os05g47840","LOC_Os01g49390","LOC_Os01g73760","LOC_Os06g51350")
  names(ret)=c("OsIPT1","OsIPT2","OsIPT3","OsIPT4","OsIPT5","OsIPT6","OsIPT7","OsIPT8","OsIPT9","OsIPT10")
  return(ret)
}
# get Cytokinin Response Factors
# From Tracy's spreadsheet: "CRFs from paper (The CRF domain defines Cytokinin Response Factor proteins in plants)"
getCRFs = function(){
  ret=c("LOC_Os05g25260","LOC_Os06g06540","LOC_Os01g12440","LOC_Os01g46870","LOC_Os07g12510","LOC_Os03g60120")
  names(ret)=c("ERF56","ERF55","ERF53","ERF54","ERF57","ERF58")
  return(ret)
}
# get the CYP735A genes (implicated in cytokinin biosynthesis)
# From Tracy's spreadsheet
getCYPs = function(){
  ret=c("LOC_Os08g33300","LOC_Os09g23820")
  names(ret)=c("CYP735A3","CYP735A4")
  return(ret)
}




makeRpkmScatterPlots = function(RPKM, sampleNames, maxToPlot=40, alpha=.4){
  plot(RPKM[sampleNames], main="RPKM", 
       pch=".", col=rgb(0,0,1,alpha), las=1,
       xlim=c(0,maxToPlot), ylim=c(0,maxToPlot))
  return(cor(RPKM[sampleNames]))
}


plotVariationFraction = function(RPKM){
  sample_types = gsub(".ave","", names(RPKM)[grep("ave", names(RPKM))])
  sd.ave = data.frame(matrix(nrow=nrow(RPKM), ncol=length(sample_types)))
  names(sd.ave) = sample_types
  for (sample_type in sample_types){
    sd=as.numeric(RPKM[,paste0(sample_type, ".sd")])
    ave=as.numeric(RPKM[,paste0(sample_type, ".ave")])
    sd.ave[,sample_type] = sd/ave
  }
  
  colors = getColors(sample_types)
  boxplot(sd.ave, main="Variation", 
          ylab="standard deviation as fraction of average", 
          las=1, col=colors)
}