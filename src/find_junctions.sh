#!/bin/bash
#PBS -l walltime=12:00:00
#PBS -l nodes=1:ppn=2
#PBS -l vmem=8000mb

module load java8

cd $PBS_O_WORKDIR

# defined by qsub
echo "sample: $SAMPLE"
echo "flank: $FLANK"
echo "input: $FILE"
echo "2bit: $TWOBIT"

JAR=/lustre/groups/lorainelab/bin/find-junctions-1.0.0-jar-with-dependencies.jar
S=$SAMPLE
JAVA=$(which java)
echo "java: $JAVA"
echo "jar: $JAR"
CMD="java -Xmx4g -jar $JAR -u -f $FLANK -b $TWOBIT -o $S.FJ.bed $FILE"
echo "running: $CMD"
$CMD
if [ -f $S.FJ.bed ]; then
    sort -k1,1 -k2,2n $S.FJ.bed | bgzip > $S.FJ.bed.gz
    tabix -s 1 -b 2 -e 3 $S.FJ.bed.gz
    rm $S.FJ.bed
fi


