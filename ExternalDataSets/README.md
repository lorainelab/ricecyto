
# What's here 

All data that is used in this study but that was not generated in this study has to come from somewhere. This module is dedicated to housing and documenting all such "external" data.

* * *

## APK_ftp_file.txt
Table of putative orthological groups downloaded from MSU website.  It was generated using OrthoMCL.
See http://rice.plantbiology.msu.edu/annotation_pseudo_apk.shtml

* * *

## AtRo.txt.gz
Differential expression data from roots from cytokinin-treated Arabidopsis thaliana seedlings.  Taken from the AtCytokinin2 repository.

* * *

## AtSh.txt.gz
Differential expression data from shoots from cytokinin-treated Arabidopsis thaliana seedlings.  Taken from the AtCytokinin2 repository.

* * *

## Msu2probeset.txt.gz
Table associating MSU7 gene ids to affymetrix probset ids from the 57K array.

* * *

## O_sativa_japonica_Oct_2011.2bit
Oryza sativa genome sequence.

* * *

## O_sativa_japonica_Oct_2011.bed.gz
Oryza sativa gene annotations.  Can be read into R or used to visualize genes in IGB.

* * *

## Osj.tsv.gz
Transcription factor annotations. Derived from
http://planttfdb.cbi.pku.edu.cn/download/gene_model_family/Osj

* * *

## RAP-MSU.txt.gz
Talbe linking RAP (RAPdb) gene ids to MSU (MSU7) gene ids.  Obtained from the Rice Annotation Project data base web site.

* * *

## RiceCytoGenes.xlsx
Collection of rice genes that have previously been shown to be involved with cytokinin.  I think this particular list is one that Yu-Chang assembled and used in Tsai et al. 2012.

* * *

## SRA_submission
Documentation of the SRA accession numbers associated with each sample and the meta data that was submitted to the SRA when the rice samples were submitted.

* * *

## TAIR10.bed.gz
Gene annotations for the Arabidosis thaliana genome.  Originally obtained from TAIR.
https://www.arabidopsis.org/

* * *

## TheGoldenList.csv
List of genes presented in Bhargava et al as being consistently, or at least frequently, regulated by cytokinin accross several independent studies.

* * *

## gene_association.gramene_oryza.gz
Table with gene info from Gramene, used for GO term associations.

* * *

## tx_size.txt.gz
This file lists the size of each rice gene. It was generated using the MSU gene annotaions.