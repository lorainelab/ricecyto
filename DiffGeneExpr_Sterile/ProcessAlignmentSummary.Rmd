---
title: "Processing tophat alignment summary"
author: "Ivory Clabaugh Blakley"
output: html_document
---

# Introduction

When tophat was run, a summary was generated for each file expressing the number of reads in the input file and the number of those that aligned. It also details how many of those reads that aligned had only one alignment (single mapping) or had multiple alignments (multimapping).  The summaries for all samples have been combined into one document.  Here, we convert that document into a table.

# Create a table from the Alignment Summary

Combine all of the alignment summary information from all samples into a single document.  This can be done using the following lines (assuming each sample was given its own directory for results):

> SET=$(ls)       
> touch AlignmentSummary.txt        
> for f in $SET; do       
> echo $f >> AlignmentSummary.txt         
> cat $f/align_summary.txt >> AlignmentSummary.txt         
> done

Read in the summary for all samples.
```{r}
as = readLines("data/AlignmentSummary.txt")
end=length(as)
head(as, 6)
```

Get each type of value for all samples.
```{r}
SampleName=as[seq(1,end,6)]

FastqReads=as[seq(3,end,6)]
FastqReads=gsub("[^.]* ", "", FastqReads)
FastqReads=as.numeric(FastqReads)

MappedReads=as[seq(4,end,6)]
MappedReads=gsub("[^.]*:[ ]*", "", MappedReads)
MappedReads=gsub("[ ]*\\([0-9.]*% of input\\)", "", MappedReads)
MappedReads=as.numeric(MappedReads)

MultiMapping=as[seq(5,end,6)]
MultiMapping=gsub("[of these:]*", "", MultiMapping)
MultiMapping=gsub("\\([^<]*", "", MultiMapping)
MultiMapping=as.numeric(MultiMapping)
```

Bring the values together in a table.
```{r}
df=data.frame(SampleName, FastqReads, MappedReads, SingleMapping=MappedReads-MultiMapping, MultiMapping)
df$PercentMapping = round(100*df$MappedReads/df$FastqReads,1)
df$PercentMultiMap = round(100*df$MultiMapping/df$MappedReads,1)
```

Write the table to a file.
```{r}
write.table(df, "results/AlignmentSummaryTable.txt", sep='\t', quote=F, row.names=F)
```

# Visualize the information

Use the short names designed for R variables.
```{r}
source("../src/rice_functions.R")
sampleNames = gsub("T", "TR", df$SampleName)
sampleNames = gsub("C", "CR", sampleNames)
row.names(df)=sampleNames
```

Plot the number of reads per file and the number aligned.
```{r echo=F}
colors=getColors(sampleNames)
```

```{r echo=F, fig.width=7, fig.height=8}
counts=as.matrix(t(df[c("FastqReads", "MappedReads", "SingleMapping")]))/1e+6 #in millions
counts=counts[,ncol(counts):1]

xlab="Reads per library (millions)"
par(las=1)
par(mar=c(3,3,1,1))
par(mgp=c(1.5,0.5,0))
barcols=c("gray", "gold", "cornflowerblue")
bp=barplot(counts,col=barcols,
           horiz=T, beside=T, names.arg=rep("", ncol(counts)),
           xlim=c(0,max(counts)+2),
           cex.names=1,main="",xlab=xlab)
text(x=0, y=bp[2,], pos=2, labels=colnames(counts), col=colors[colnames(counts)], xpd=T)
box()
text(par("usr")[2]/2,par("usr")[4],"Sequencing Depth", adj=c(.5,-.2), cex=1.2, xpd=T)
legend(x="bottomright", legend=row.names(counts)[3:1], fill=barcols[3:1])
fname="results/SequencingDepth.png"
print(paste("image saved as", fname))
quartz.save(file=fname, dpi=100)
```

 * Each fastq file contained `r round(min(df$FastqReads)/1e+6,1)` to `r round(max(df$FastqReads)/1e+6,1)` million reads.
 * Each file included `r round(min(df$MappedReads)/1e+6,1)` to `r round(max(df$MappedReads)/1e+6,1)` million reads that aligned to the Arabidopsis (Col-0) genome.
 * This includes `r round(min(df$SingleMapping)/1e+6,1)` to `r round(max(df$SingleMapping)/1e+6,1)` million single mapping reads per sample.
 
 
Plot the percentage of aligned reads per file.

```{r echo=F, fig.width=6}
pa=as.matrix(t(df[c("PercentMapping")]))
pa=as.matrix(pa[,ncol(pa):1])

xlab="% reads aligned"
par(las=1)
par(mar=c(3,3,1,1))
par(mgp=c(1.5,0.5,0))
bp=barplot(pa,col=colors[rownames(pa)],
           horiz=T, beside=T, names.arg=rep("", length(pa)),
           xlim=c(0,100),
           cex.names=1,main="",xlab=xlab)
text(x=0, y=bp, pos=2, labels=rownames(pa), col=colors[rownames(pa)], xpd=T)
box()
text(par("usr")[2]/2,par("usr")[4],"Percent Alignment", adj=c(.5,-.2), cex=1.2, xpd=T)
avg=round(mean(pa),2)
abline(v=avg, lty=2)
fname="results/PercentAlignment.png"
print(paste("image saved as", fname))
quartz.save(file=fname, dpi=100)
```

 * In each sample, `r min(pa)` to `r max(pa)`% of all reads aligned to the reference genome.
 * The mean, (`r avg`%) is shown as a dotted gray line.
 
 

 
Session info
```{r}
sessionInfo()
```


