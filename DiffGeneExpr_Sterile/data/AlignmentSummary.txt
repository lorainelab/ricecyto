C1
Reads:
          Input     :  17931836
           Mapped   :  16285350 (90.8% of input)
            of these:    846122 ( 5.2%) have multiple alignments (5 have >20)
90.8% overall read mapping rate.
C2
Reads:
          Input     :  19669326
           Mapped   :  17922351 (91.1% of input)
            of these:    879795 ( 4.9%) have multiple alignments (2 have >20)
91.1% overall read mapping rate.
C3
Reads:
          Input     :  18733557
           Mapped   :  17206540 (91.8% of input)
            of these:   1203726 ( 7.0%) have multiple alignments (2 have >20)
91.8% overall read mapping rate.
T1
Reads:
          Input     :  24110189
           Mapped   :  21139439 (87.7% of input)
            of these:    913094 ( 4.3%) have multiple alignments (1 have >20)
87.7% overall read mapping rate.
T2
Reads:
          Input     :  21668062
           Mapped   :  19603396 (90.5% of input)
            of these:    868631 ( 4.4%) have multiple alignments (2 have >20)
90.5% overall read mapping rate.
T3
Reads:
          Input     :  19342054
           Mapped   :  17411509 (90.0% of input)
            of these:    944177 ( 5.4%) have multiple alignments (3 have >20)
90.0% overall read mapping rate.
