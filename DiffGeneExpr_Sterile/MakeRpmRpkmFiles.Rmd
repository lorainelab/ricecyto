Creating RPM and RPKM files
===========================


```{r echo=FALSE}
library(knitr)
#knitr::opts_chunk$set(fig.path = 'figure/')
knit_hooks$set(inline = function(x) {
  prettyNum(x, big.mark=",", digits=2)
})
```

Introduction
------------
This file creates RPM (counts per million) and RPKM (counts per million thousand) with expression data for every annotated gene.

The RPM file is useful for comparing expression across samples and the RPKM file is useful comparing expression between genes within the same sample. 

Questions:

* How many reads did we map onto the reference rice genome?
* How many genes were expressed with average 0.5, 1, and 5 RPKM in at least one sample type?

### Load data

Load the counts data:

```{r}
source('../src/rice_functions.R')
fname='data/roots2_counts.tsv.gz'
counts=getCounts2(fname)

names(counts)=sub('T','TR',names(counts))
names(counts)=sub('C','CR',names(counts))
```

Get annotations:

```{r}
annotsfile='../ExternalDataSets/O_sativa_japonica_Oct_2011.bed.gz'
annots=read.delim(annotsfile,header=T,sep='\t',quote="")[,c(13,14)]
names(annots)=c('gene','descr')
annots=annots[!duplicated(annots$gene),]
rownames(annots)=annots$gene
```

Read mapping info:

```{r}
fname='results/AlignmentSummaryTable.txt'
mapped=read.delim(fname,sep='\t',header=T,as.is=T)

# Change sample names to match counts
mapped$sample=sub('T','TR',mapped$SampleName)
mapped$sample=sub('C','CR',mapped$sample)
row.names(mapped)=mapped$sample
```

We mapped `r round(sum(mapped$MappedReads)/10**6,1)` million reads onto the rice genome, with `r round(sum(mapped$SingleMapping)/10**6,1)` million reads mapping exactly once.


Mapped reads per sample:

```{r}
millions=mapped["SingleMapping"]
millions$count=round(millions$SingleMapping/10**6,1)
```
```{r echo=FALSE}
kable(millions["count"], col.names="MillionSingleMappingReads")
```


### Make RPM table

Calculate RPM and merge with annotations:

```{r}
RPM=counts
for (sample in mapped$sample) {
  v=RPM[,sample]/millions[sample,'count']
  RPM[,sample]=v
}
sample_types=unique(sub('\\d','',mapped$sample))
for (sample_type in sample_types) {
  cols=grep(sample_type,names(RPM))
  ave=apply(RPM[,cols],1,mean)
  RPM=cbind(RPM,ave)
  names(RPM)[ncol(RPM)]=paste0(sample_type,'.ave')
  sd=apply(RPM[,cols],1,sd)
  RPM=cbind(RPM,sd)
  names(RPM)[ncol(RPM)]=paste0(sample_type,'.sd')
}
to_write=RPM
to_write$gene=row.names(RPM)
to_write=merge(to_write,annots,by.x='gene',
               by.y='gene')
```

Write results:

```{r}
f='results/riceSterile_RPM.tsv'
write.table(to_write,file=f,row.names=F,
            sep='\t',quote=F)
system(paste('gzip -f',f))
```

### Make RPKM table

Get gene sizes. this information can be read from the same counts file we used earlier.
```{r}
fname='data/roots2_counts.tsv.gz'
d=read.delim(fname,header=T,sep='\t', comment.char="#")
rownames(d)=d$Geneid
sizes = d$Length
names(sizes) = row.names(d)
sizes = sizes/1000
```


Divide by size (kb):

```{r}
RPKM=RPM[,names(counts)]
for (sample in names(RPKM)) {
  RPKM[,sample]=RPKM[,sample]/sizes
}
for (sample_type in sample_types) {
  cols=grep(sample_type,names(RPKM))
  ave=apply(RPKM[,cols],1,mean)
  RPKM=cbind(RPKM,ave)
  names(RPKM)[ncol(RPKM)]=paste0(sample_type,'.ave')
  sd=apply(RPKM[,cols],1,sd)
  RPKM=cbind(RPKM,sd)
  names(RPKM)[ncol(RPKM)]=paste0(sample_type,'.sd')
}
to_write=RPKM
to_write$gene=row.names(RPKM)
to_write=merge(to_write,annots,by.x='gene',
               by.y='gene')
```


Write data file:

```{r}
f='results/riceSterile_RPKM.tsv'
write.table(to_write,file=f,row.names=F,
            sep='\t',quote=F)
system(paste('gzip -f',f))
```

### Investigate expression 

```{r}
v=c(0.5,1,5,20)
cnames=sapply(sample_types,function(x){paste0(x,'.ave')})
mat1=matrix(ncol=length(cnames),nrow=length(v))
mat2=matrix(ncol=length(cnames),nrow=length(v))
colnames(mat1)=cnames
colnames(mat2)=cnames
rownum=1
for (i in v){
  n=apply(RPKM[,cnames],2,function(x){sum(x>=i)})
  mat1[rownum,]=n
  mat2[rownum,]=n/nrow(RPKM)*100
  rownum=rownum+1
}
rownames(mat1)=v
rownames(mat2)=v
```

Table showing the number of genes (out of `r nrow(RPKM)`) with RPKM expression levels of at least the value indicated by row names:

```{r}
mat1
```

Table showing the percentage of genes (out of `r nrow(RPKM)`) with RPKM expression levels of at least the value indicated by row names:

```{r}
mat2
```

### Variation between samples

Variation in the RPKM values.  Most genes (all but `r rowMeans(mat2)['20']`%) have RPKM of less than 20. The genes with extremely high expression (>40) are not shown in the plot.
```{r RpkmScatterPlots, fig.path="results/figure/"}
cp=makeRpkmScatterPlots(RPKM, sampleNames = names(counts))
```
```{r echo=F}
kable(cp)
```

Calculate standard deviation as a fraction of the average. Show as a boxplot for each sample type.
```{r VariationFraction, fig.width=4, fig.height=4, fig.path="results/figure/"}
plotVariationFraction(RPKM)
```


Conclusion
----------

* How many reads did we map onto the reference rice genome?

We mapped `r round(sum(millions$count),1)` million reads onto the rice genome.

* How many genes were expressed with average 0.5, 1, and 5 RPKM in at least one sample type?

Around `r rowMeans(mat2)['0.5']`% of genes were expressed at or above 0.5 RPKM, `r rowMeans(mat2)['1']`% at or above 1 RPKM, `r rowMeans(mat2)['5']`% at or above 5 RPKM, and `r rowMeans(mat2)['20']`% at or above 20 RPKM.

We wrote the RPM and RPKM data files needed for other analyses.

Limitations
-----------

* Some reads from overlapping genes were counted more than once. However, these were not included in the denominators used to normalize counts and output RPM and RPKM files. 

Session information
-------------------

```{r}
sessionInfo()
````

