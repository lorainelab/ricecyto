---
title: "Representation of Orthologous groups"
author: "Ivory Clabaugh Blakley"
date: "March 10, 2015"
output:
  html_document:
    toc: true
---

# Introduction


## Questions    
* Which Orthologous groups are represented in the rice roots and shoots DE genes?
* Which Orthologous groups are represented in the Arabidopsis roots and shoots DE genes?
* Are all At/Os genes from a given group changed the same way in a given gene set?
* Which groups are shared/divergent between the rice and Arabidopsis data sets?

```{r}
source("../src/rice_functions.R")
suppressPackageStartupMessages(library("Vennerable"))
atCol = "#DC4A48"
osCol = "#469A98"
cols = c(atCol, osCol)
Osfdr = getFDR()
Atfdr = getAtFDR()
```
We will call rice genes differentially expressed if they have an fdr of `r Osfdr` or better, and Arabidopsis genes if they have an fdr of `r Atfdr` or better.

# Orthologous groups
Read in the data set that was downloaded through the MSU Rice Genome Annotation Project: http://rice.plantbiology.msu.edu/annotation_pseudo_apk.shtml
They offer the following explaination:
"A total of 232,821 representative peptide sequences from rice* (release 7), Arabidopsis (release 10), poplar (release 2.2), grapevine (release 1_12x), sorghum (release 1.4), maize (release 5b filtered set), and Brachypodium (release 1.0) were used for identification of putative orthologous groups using OrthoMCL with default parameters (Li et al., 2003). A total of 29,968 putative orthologous groups including in-paralogs were identified."
```{r}
fname="../ExternalDataSets/APK_ftp_file.txt"
og = read.delim(fname, as.is=T) # og for orthologous groups
row.names(og) = og$orthologous_group_ID
dim(og)
```

#### How many orthologous groups are shared between rice and Arabidopsis?
```{r}
At = og[grep("arabidopsis", og$species), "orthologous_group_ID"]
Os = og[grep("rice", og$species), "orthologous_group_ID"]
all = length(union(At, Os))
both = length(intersect(At, Os))
```
Of the `r all` groups that have members from either Arabiodopsis or rice, `r both` of them have members from both species.

```{r echo=FALSE}
weights=c(length(At)-both, 
          length(Os)-both, 
          both)
names(weights) = c("10", "01", "11")
V=Venn(Weight=weights, SetNames=c("At", "Os")) #an object of class Venn to use with Vennerable package

C2 = compute.Venn(V, doWeights = T) 
theme = VennThemes(C2) 
for (i in 1:2){
  theme$SetText[[i]]$col = cols[i]
  theme$Set[[i]]$col = cols[i]
  theme$Set$lwd = 5
  theme$SetText[[i]]$fontsize = 14
}
#C2@SetLabels$hjust[2] = "left"
#C2@SetLabels$hjust[1] = "right"

grid.newpage()
plot(C2, gpList = theme, show=list(Faces=F))
```

We can limit the list of orthologous groups to just the groups that have Arabidopsis and/or rice members. 
```{r}
og = og [union(At, Os),]
ogL = strsplit(og$orthologs.in.paralogs, split=" ") # ogL for orthologous group list
names(ogL) = og$orthologous_group_ID
```
```{r echo=FALSE}
rm(og) #save on memory once we're done with an object
```


Set up the results file.
```{r}
df=data.frame(matrix(nrow=all, ncol=7))
names(df)=c("GroupName", "OsGenes", "OsRoot", "OsShoot", "AtGenes", "AtRoot", "AtShoot")
df$GroupName = union(At, Os)
row.names(df)=df$GroupName

#Possible values for the AtRoot, AtShoot, OsRoot and OsShoot columns:
#"NA" there is no ortholog in this group or it is not expressed 
notDetected = "NotDetected" #there is at least one group memeber but none were detected with at least one RNA-seq read.
detected="detected" # At least one gene from the group was detected, but not DE
upReg="UpReg" #At least one gene was DE and all DE genes were up regulated.
downReg="DownReg" #At least one gene was DE and all DE genes were down regulated.
conf="conflicted" #More than one gene from the group was DE, and changed in opposite directions.
noAtOrth = "No At ortholog"
noOsOrth = "No Os ortholog"
```

Fill in the resutls file with the genes in each group for each species. 
```{r}
#Arabidopsis
atMembers = lapply(ogL, FUN=grep, pattern="AT", value=T)
atMembers = sapply(atMembers, paste0, collapse="|")
df[names(atMembers),"AtGenes"] = unlist(atMembers)
#rice
osMembers = lapply(ogL, FUN=grep, pattern="LOC_Os", value=T)
osMembers = sapply(osMembers, paste0, collapse="|")
df[names(osMembers),"OsGenes"] = unlist(osMembers)
```



## Orthologous groups in Arabidopsis
Now we need to have some data from Arabidopsis roots and shoots.  We'll pull out the set of genes that are differentially expressed with a false discovery rate of `r Atfdr`.  Then we'll see which groups are represented in that set of genes.

### Arabidopsis roots
Read in the results file for arabidopsis roots.  Pull out the genes that are DE based on an fdr of `r Atfdr`.
```{r ArabidopsisRoots}
fname="../ExternalDataSets/AtRo.txt.gz"
atro = read.delim(fname)
de = which(atro$fdr<=Atfdr)
atroDE = atro[de,"logFC"]
names(atroDE) = atro$gene[de]
```

#### How many groups
How many of the orthologous groups have at least one member in this set of DE genes?
```{r}
##Create a function to do this step over and over. Use the following arguments.
#ogL - a list where each item is a vector of gene names for a given group, and the item names are the group names
#ogSet - vector of group names (subset of names(ogL) to consider
#DEgeneSet - named vector with fold changes as the values and gene ids as the names
#species - character string, fill in with "rice" or "Arabidopsis", used in printed message
#comparison - character string, fill in with "roots" or "shoots", used in printed message
#printMessage - T/F should the summary message be printed.
checkGroupRepresentation = function(ogL, ogSet, DEgeneSet, species="fill in", comparison="fill in", printMessage=T){
  ogList = as.list(ogSet)
  names(ogList) = ogSet
  for (orth in ogSet){
    groupMem = ogL[[orth]] # get the members of the orthologous group
    DEmem = intersect(groupMem, names(DEgeneSet)) # get the names of genes that are group members and DE genes
    DEmem = DEgeneSet[DEmem] # get the logFC values as a named vector for those genes
    ogList[[orth]] = DEmem
    }
  w = which(lapply(ogList, length) > 0)
  ogList = ogList[w]
  if(printMessage){print(paste0("Of the ", length(ogSet), " orthologous groups that have at least one member from ", species, ", ", length(w), " of them are represented in the list of genes that are DE in the ", comparison, " comparison."))}
  return(ogList)
  }
# Use the function we have just defined.
ogL.AtR = checkGroupRepresentation(ogL=ogL, ogSet=At, DEgeneSet=atroDE, 
                                  species="Arabidopsis",
                                  comparison="Arabidopsis root")
```

Fill in the Arabidopsis root section of the results file. (not shown)
```{r echo=FALSE}
#Note all of the group that at least have an At memeber
df[At,"AtRoot"]=notDetected #most of these will be overwritten later
#Note all of the groups with at least one detected memeber
genesDetInAtRoot = atro$gene
names(genesDetInAtRoot)=genesDetInAtRoot
genesDetInAtRoot = checkGroupRepresentation(ogL=ogL, ogSet=At, DEgeneSet=genesDetInAtRoot, printMessage=F)
df[names(genesDetInAtRoot),"AtRoot"]="detected"

#Note all of the groups with at least 1 DE memeber
df[names(ogL.AtR),"AtRoot"]="conflicting direction" #most of these will be overwritten later
```


#### Consistent behavior within groups
Within this set of orthologous groups that are represented in the arabiodopsis root DE list, are there any groups that include more than one DE gene?
```{r}
numPerGroup = sapply(ogL.AtR, length)
hist(numPerGroup, labels=T, ylim=c(0,length(ogL.AtR)), 
     breaks=seq(.5, max(numPerGroup)+.5,1),
     xlab="Group members in At root DE genes list",
     main="orthologous group representation", col=atCol)
```

Most represented groups only have one gene on the DE genes list, but there are `r sum(numPerGroup>1)` groups with more than one DE member.   
If there are multiple DE genes from the same orthologous groups, we would expect that they would all be changed in the same direction; is that true?
```{r}
checkConsistentBehavior = function(ogList){
  #change all the positive logFC values to 1, and negative logFC values to -1.
  for (orth in names(ogList)){
    ogList[[orth]][which(ogList[[orth]]>0)] = 1 
    ogList[[orth]][which(ogList[[orth]]<0)] = -1 
    ogList[[orth]] = unique(ogList[[orth]])
    }
  conflicting = sum(sapply(ogList, length)>1)
  print(paste("There are", conflicting, "groups with members regulated in opposite directions."))
if (conflicting > 0){
  print("Those have been removed from the list.")
}
  w=which(sapply(ogList, length)==1)
  ogList = ogList[w]
  return(ogList)
  }
ogL.AtR = checkConsistentBehavior(ogL.AtR)
```

Fill in the Arabidopsis root direction of change in the results file. (not shown)
```{r echo=FALSE}
#Note all of the groups with at least 1 DE memeber and consistent direction
df[names(ogL.AtR),"AtRoot"]=unlist(ogL.AtR)
df[which(df$AtRoot=="-1"),"AtRoot"] = downReg
df[which(df$AtRoot=="1"),"AtRoot"] = upReg
```


### Arabidopsis shoots
Read in the results file for arabidopsis shoots.  Pull out the genes that are DE based on an fdr of `r Atfdr`.
```{r ArabidopsisShoots}
fname="../ExternalDataSets/AtSh.txt.gz"
atsh = read.delim(fname)
de = which(atsh$fdr<=Atfdr)
atshDE = atsh[de,"logFC"]
names(atshDE) = atsh$gene[de]
```

#### How many groups
How many of the orthologous groups have at least one member in this set of DE genes?
```{r}
ogL.AtS = checkGroupRepresentation(ogL=ogL, ogSet=At, DEgeneSet=atshDE, 
                                  species="Arabidopsis",
                                  comparison="Arabidopsis shoot")
```

Fill in the Arabidopsis shoot section of the results file. (not shown)
```{r echo=FALSE}
#Note all of the groups that at least have an At memeber
df[At,"AtShoot"]=notDetected #most of these will be overwritten later
#Note all of the groups with at least one detected memeber
genesDetInAtShoot = atsh$gene
names(genesDetInAtShoot)=genesDetInAtShoot
genesDetInAtShoot = checkGroupRepresentation(ogL=ogL, ogSet=At, DEgeneSet=genesDetInAtShoot, printMessage=F)
df[names(genesDetInAtShoot),"AtShoot"]="detected"

#Note all of the groups with at least 1 DE memeber
df[names(ogL.AtS),"AtShoot"]="conflicting direction" #most of these will be overwritten later
```


#### Consistent behavior within groups
Within this set of orthologous groups that are represented in the arabiodopsis shoot DE list, are there any groups that include more than one DE gene?
```{r}
numPerGroup = sapply(ogL.AtS, length)
hist(numPerGroup, labels=T, ylim=c(0,length(ogL.AtS)+3), 
     breaks=seq(.5, max(numPerGroup)+.5,1),
     xlab="Group members in At shoot DE genes list",
     main="orthologous group representation", col=atCol)
```
There are `r sum(numPerGroup>1)` groups with more than one DE member.   
If there are multiple DE genes from the same orthologous groups, we would expect that they would all be changed in the same direction; is that true?
```{r}
ogL.AtS = checkConsistentBehavior(ogL.AtS)
```

Fill in the Arabidopsis shoot direction of change in the results file. (not shown)
```{r echo=FALSE}
#Note all of the groups with at least 1 DE memeber and consistent direction
df[names(ogL.AtS),"AtShoot"]=unlist(ogL.AtS)
df[which(df$AtShoot=="-1"),"AtShoot"] = downReg
df[which(df$AtShoot=="1"),"AtShoot"] = upReg
```

Make a set of genes that were DE in Arabidopsis in either part of the plant.
```{r}
AtDEgenes = union(atro[atro$fdr<=Atfdr,"gene"], atsh[atsh$fdr<=Atfdr,"gene"])
#remove objects that we don't need
rm(atro)
rm(atsh)
```


## Orthologous groups in rice
Now we need to have some data from rice roots and shoots.  We'll pull out the set of genes that are differentially expressed with a false discovery rate of `r Osfdr`.  Then we'll see which groups are represented in that set of genes.

### Rice roots
Read in the results file for rice roots.  Pull out the genes that are DE based on an fdr of `r Osfdr`.
```{r riceRoots}
fname="../DiffGeneExpr/results/R.tsv.gz"
osro = read.delim(fname)
de = which(osro$fdr<=Osfdr)
osroDE = osro[de,"logFC"]
names(osroDE) = osro$gene[de]
```

#### How many groups
How many of the orthologous groups have at least one member in this set of DE genes?
```{r}
ogL.OsR = checkGroupRepresentation(ogL=ogL, ogSet=Os, DEgeneSet=osroDE, 
                                  species="rice",
                                  comparison="rice root")
```


Fill in the rice root section of the results file. (not shown)
```{r echo=FALSE}
#Note all of the groups that have at least one Os memeber
df[Os,"OsRoot"]=notDetected #most of these will be overwritten later
#Note all of the groups with at least one detected Os memeber
genesDetInOsRoot = osro$gene
names(genesDetInOsRoot)=genesDetInOsRoot
genesDetInOsRoot = checkGroupRepresentation(ogL=ogL, ogSet=Os, DEgeneSet=genesDetInOsRoot, printMessage=F)
df[names(genesDetInOsRoot),"OsRoot"]="detected"

#Note all of the groups with at least one DE Os memeber
df[names(ogL.OsR),"OsRoot"]="conflicting direction" #most of these will be overwritten later
```


#### Consistent behavior within groups
Within this set of orthologous groups that are represented in the rice root DE list, are there any groups that include more than one DE gene?
```{r}
numPerGroup = sapply(ogL.OsR, length)
hist(numPerGroup, labels=T, ylim=c(0,length(ogL.OsR)), 
     breaks=seq(.5, max(numPerGroup)+.5,1),
     xlab="Group members in rice root DE genes list",
     main="orthologous group representation", col=osCol)
```

Most represented groups only have one gene on the DE genes list, but there are `r sum(numPerGroup>1)` groups with more than one DE member.     
If there are multiple DE genes from the same orthologous groups, we would expect that they would all be changed in the same direction; is that true?
```{r}
ogL.OsR = checkConsistentBehavior(ogL.OsR)
```


Fill in the rice root direction of change in the results file. (not shown)
```{r echo=FALSE}
#Note all of the groups with at least 1 DE memeber and consistent direction
df[names(ogL.OsR),"OsRoot"]=unlist(ogL.OsR)
df[which(df$OsRoot=="-1"),"OsRoot"] = downReg
df[which(df$OsRoot=="1"),"OsRoot"] = upReg
```



### Rice shoots
Read in the results file for rice shoots.  Pull out the genes that are DE based on an fdr of `r Osfdr`.
```{r riceShoots}
fname="../DiffGeneExpr/results/S.tsv.gz"
ossh = read.delim(fname)
de = which(ossh$fdr<=Osfdr)
osshDE = ossh[de,"logFC"]
names(osshDE) = ossh$gene[de]
```


#### How many groups
How many of the orthologous groups have at least one member in this set of DE genes?
```{r}
ogL.OsS = checkGroupRepresentation(ogL=ogL, ogSet=Os, DEgeneSet=osshDE, 
                                  species="rice",
                                  comparison="rice shoot")
```



Fill in the rice shoots section of the results file. (not shown)
```{r echo=FALSE}
#Note all of the groups that at least have an Os memeber
df[Os,"OsShoot"]=notDetected #most of these will be overwritten later
#Note all of the groups with at least one detected memeber
genesDetInOsShoot = ossh$gene
names(genesDetInOsShoot)=genesDetInOsShoot
genesDetInOsShoot = checkGroupRepresentation(ogL=ogL, ogSet=Os, DEgeneSet=genesDetInOsShoot, printMessage=F)
df[names(genesDetInOsShoot),"OsShoot"]="detected"

#Note all of the groups with at least 1 DE memeber
df[names(ogL.OsS),"OsShoot"]="conflicting direction" #most of these will be overwritten later
```



#### Consistent behavior within groups
Within this set of orthologous groups that are represented in the rice shoot DE list, are there any groups that include more than one DE gene?
```{r}
numPerGroup = sapply(ogL.OsS, length)
hist(numPerGroup, labels=T, ylim=c(0,length(ogL.OsS)), 
     breaks=seq(.5, max(numPerGroup)+.5,1),
     xlab="Group members in rice root DE genes list",
     main="orthologous group representation", col=osCol)
```


Most represented groups only have one gene on the DE genes list, but there are `r sum(numPerGroup>1)` groups with more than one DE member.     
If there are multiple DE genes from the same orthologous groups, we would expect that they would all be changed in the same direction; is that true?
```{r}
ogL.OsS = checkConsistentBehavior(ogL.OsS)
```


Fill in the rice shoot direction of change in the results file. (not shown)
```{r echo=FALSE}
#Note all of the groups with at least 1 DE memeber and consistent direction
df[names(ogL.OsS),"OsShoot"]=unlist(ogL.OsS)
df[which(df$OsShoot=="-1"),"OsShoot"] = downReg
df[which(df$OsShoot=="1"),"OsShoot"] = upReg
```

Make a set of genes that were DE in rice in either part of the plant
```{r}
OsDEgenes = union(osro[osro$fdr<=Osfdr,"gene"], ossh[ossh$fdr<=Osfdr,"gene"])
#remove large objects that we are done with
rm(osro)
rm(ossh)
```



## Shared/Unshared groups between At and Os

### How many groups have DE genes in both root data sets

```{r echo=FALSE}
V=Venn(list(AllSharedGroups = intersect(At, Os),
            AtRoot = names(ogL.AtR),
            OsRoot = names(ogL.OsR)))
C3 = compute.Venn(V, doWeights = F) 
theme = VennThemes(C3) 
cols3 = c(gray(.4), cols)
for (i in 1:3){
  theme$SetText[[i]]$col = cols3[i]
  theme$Set[[i]]$col = cols3[i]
  theme$Set$lwd = 5
  theme$SetText[[i]]$fontsize = 14
}
sharedOrthR = V@IntersectionSets$'111'
grid.newpage()
plot(C3, gpList = theme, show=list(Faces=F))
```
There are `r length(sharedOrthR)` orthologous groups that are represented in the root DE genes in both rice and Arabidopsis.  Of the groups that are only represented in one species, about half of them do not have an orthologe in the other species.  This may be a sign of dramatic divergence, or (more likely) an indication that the annotations we have for orthologies are not complete.

#### Do orthologs behave similarly across species
Where an orthological group is represented in both species, is the change in the same direction?
```{r}
names(sharedOrthR) = sharedOrthR
for (orth in sharedOrthR){
  sharedOrthR[orth] = ifelse(ogL.AtR[[orth]] * ogL.OsR[[orth]] ==1, "same", "opposite")
}
table(sharedOrthR)
```

### How many groups have DE genes in both shoot data sets
```{r echo=FALSE}
V=Venn(list(AllSharedGroups = intersect(At, Os),
            AtShoot = names(ogL.AtS),
            OsShoot = names(ogL.OsS)))
C3 = compute.Venn(V, doWeights = F) 
theme = VennThemes(C3) 
cols3 = c(gray(.4), cols)
for (i in 1:3){
  theme$SetText[[i]]$col = cols3[i]
  theme$Set[[i]]$col = cols3[i]
  theme$Set$lwd = 5
  theme$SetText[[i]]$fontsize = 14
}
sharedOrthS = V@IntersectionSets$'111'
grid.newpage()
plot(C3, gpList = theme, show=list(Faces=F))
```

#### Do orthologs behave similarly across species
Where an orthological group is represented in both species, is the change in the same direction?
```{r}
names(sharedOrthS) = sharedOrthS
for (orth in sharedOrthS){
  sharedOrthS[orth] = ifelse(ogL.AtS[[orth]] * ogL.OsS[[orth]] ==1, "same", "opposite")
}
table(sharedOrthS)
```


### Are groups most often shared between sets from the same tissue or same species?
```{r echo=FALSE}
V=Venn(list(#AllSharedGroups = intersect(At, Os),
            AtRoot = names(ogL.AtR),
            AtShoot = names(ogL.AtS),
            OsRoot = names(ogL.OsR),
            OsShoot = names(ogL.OsS)))
C4 = compute.Venn(V, doWeights = F) 
theme = VennThemes(C4) 
cols4 = rep(cols, each=2)
for (i in 1:4){
  theme$SetText[[i]]$col = cols4[i]
  theme$Set[[i]]$col = cols4[i]
  theme$Set$lwd = 5
  theme$SetText[[i]]$fontsize = 14
}

grid.newpage()
plot(C4, gpList = theme, show=list(Faces=F)) 
```

The most apparent take away message is that the At shoot set is really tiny, the At root set is larger, the rice shoot is larger than that and the Os root set is way larger. 

I think this data might be more meaningful if I used a different p-value for each data set so that the number of DE genes in each group was within an order of magnetude of each other--at least for the three sets that have an ample number.  I think this is a valid thing to try because this is ultimately a qualitative comparison, and it would be better to look at the relationships between the top 500 DE genes in each group than it would be to drown out the top genes with the fact that maybe one study had more statistical power than the other.

The number of orthologs in each section is not nearly as interesting as the patterns in each section--we know there ARE conserved pathways, WHAT pathways are they? 

I assume the one ortholg shared by all four sets represents one or more of the type As, but I need to check on that assumption.  

The At shoot set has so many fewer genes than the others that I think some things will be clearer if we only consider the other three sets.
```{r echo=FALSE}
V=Venn(list(AtRoot = names(ogL.AtR),
            OsShoot = names(ogL.OsS),
            OsRoot = names(ogL.OsR)))
C3 = compute.Venn(V, doWeights = T) 
theme = VennThemes(C3) 
colsAOO = c(atCol, osCol, osCol)
for (i in 1:3){
  theme$SetText[[i]]$col = colsAOO[i]
  theme$Set[[i]]$col = colsAOO[i]
  theme$Set$lwd = 5
  theme$SetText[[i]]$fontsize = 14
}
# sharedOrthS = V@IntersectionSets$'111'
grid.newpage()
plot(C3, gpList = theme, show=list(Faces=F)) 
```

# Fill out results file
###Which groups can be compared accross species.
Add columns indicating if the orthologous group can be used in a comparison between the species, one for root and one for shoot.
```{r}
#To start, let all the values say that both were detected then change the appropriate values to reflect groups that have not member in one species, or no detecteded members.
df$CompareShoots = "DetectedInBoth"
df$CompareRoots = "DetectedInBoth"
```

Indicate if a group was detected in the same part of the plant in both species.
```{r}
#Root
noAtDet=which(df$AtRoot == notDetected )
noOsDet=which(df$OsRoot == notDetected )
df$CompareRoots[setdiff(noAtDet, noOsDet)] = "no At gene detected"
df$CompareRoots[setdiff(noOsDet, noAtDet)] = "no Os gene detected"
df$CompareRoots[intersect(noAtDet, noOsDet)] = "none detected"

#Shoot
noAtDet=which(df$AtShoot == notDetected )
noOsDet=which(df$OsShoot == notDetected )
df$CompareShoots[setdiff(noAtDet, noOsDet)] = "no At gene detected"
df$CompareShoots[setdiff(noOsDet, noAtDet)] = "no Os gene detected"
df$CompareShoots[intersect(noAtDet, noOsDet)] = "none detected"
```
Indicate if a group did not have an ortholog in one species.
```{r}
df[setdiff(At,Os),c("CompareShoots", "CompareRoots", "OsRoot", "OsShoot")]=noOsOrth
df[setdiff(Os,At),c("CompareShoots", "CompareRoots", "AtRoot", "AtShoot")]=noAtOrth
```

### Add gene descriptions
For each species, define the set of genes that were differentially expressed in at least one part of the plant and add the gene descriptions for those genes only. In the few cases where multiple genes from a group are differentially expressed, use both descriptions with a "|" between them, and any descriptions that appear more often should come first.
```{r}
## Use this function to get the gene descriptions for a set of genes
# genes - vector of gene ids
# annots - data frame with column "gene" for gene ids and "descr" for gene descriptions
# deGenes - only return descriptsions for genes that are appear in this set
getDescriptions=function(genes, annots, deGenes){
  #only consider the descriptions of genes that were DE
  genes=intersect(genes, deGenes)
  #Get the description for each gene in the group
  desc=table(annots[genes,"descr"])
  #decriptions that are duplicated more times should appear first, only list each description once.
  ord=order(desc, decreasing=T)
  desc=names(desc)[ord]
  return(desc)
}
```

Add gene descriptions for differentially expressed Arabidopsis genes.
```{r echo=FALSE}
# The arabiopsis annotations file may not exist in your file system.
# It is version controled elsewhere, and is not part of the rice repository
# This code chunk will check for it, and download it if needed.
fname="../ExternalDataSets/TAIR10.bed.gz"
if (!file.exists(fname)){
  url="http://www.igbquickload.org/A_thaliana_Jun_2009/TAIR10.bed.gz"
  print("Downloading TAIR10 gene annotations.")
  download.file(url=url, destfile=fname)
}
```
```{r}
#fname="../ExternalDataSets/TAIR10.bed.gz"
annots=read.delim(fname, header=F, as.is=T)[,c(4,14)]
names(annots)=c("gene", "descr")
annots$gene = gsub("\\.[0123456789]*", "", annots$gene)
annots = annots[!duplicated(annots$gene),]
row.names(annots)=annots$gene

atMembers = lapply(ogL, FUN=grep, pattern="AT", value=T)
#remove any where the group had no At members
atMembers = atMembers[lapply(atMembers, length)>0] 
atMembers = lapply(atMembers, getDescriptions, annots=annots, deGenes=AtDEgenes)
#remove any where the group had no DE members
atMembers = atMembers[lapply(atMembers, length)>0]
atMembers = sapply(atMembers, paste0, collapse="|")

df$AtDEgeneDescr=""
df[names(atMembers),"AtDEgeneDescr"]= atMembers

# rm(annots)
```

Add gene descriptions for differentially expressed rice genes.
```{r}
annots=getAnnots()

osMembers = lapply(ogL, FUN=grep, pattern="LOC_Os", value=T)
osMembers = osMembers[lapply(osMembers, length)>0]
osMembers = lapply(osMembers, getDescriptions, annots=annots, deGenes=OsDEgenes)
osMembers = sapply(osMembers, paste0, collapse="|")

df$OsDEgeneDescr=""
df[names(osMembers),"OsDEgeneDescr"] = osMembers

rm(annots)
```

### Write to file
```{r}
fname="results/AtOsOrthology.txt"
write.table(df, fname, sep="\t", quote=T, row.names=F, col.names=T)
system(paste("gzip -f", fname))
```


# Conclusion


***Session Info and External Functions***
```{r}
sessionInfo()
```

