# Arabidopsis and rice comparative orthology results files #

In the OrthGroups module, there are two mark downs.  The first, OrthGroups.Rmd, uses the orthologous group assignments downloaded from MSU to make several observations about the rice and Arabidopsis results in terms of __groups__ that include differentially expressed genes in one or both data sets.  The second mark down, PortionOfDEGenes.Rmd, looks at the proportion of all DE __genes__ in each set in terms of how the __ortholog(s) of each gene__ are represented (up regulated, down regulated, not regulated) in the other data set.

* * *

## AtOsOrthology.txt.gz 

File produced by OrthGroups.Rmd, describes all orthologous groups that have at least one member (gene) in rice or Arabidopsis.      
This file includes the following columns:  

 * __GroupName__ - Name of the group of orthologous terms
 * __OsGenes__ - pipe ("|") separated list of rice genes in group
 * __OsRoot__ - status of any DE genes in the rice root data set. "conflicting direction" indicates that there were one or more genes that were regulated in each direction.
 * __OsShoot__ - status of any DE genes in the rice shoot data set.  	  
 * __AtGenes__ - pipe ("|") separated list of Arabidopsis genes in group  	  
 * __AtRoot__ - status of any DE genes in the Arabidopsis root data set.  	  
 * __AtShoot__ - status of any DE genes in the Arabidopsis shoot data set.  	  
 * __CompareShoots__ - indicates if there enough data to make a comparison for this term between the rice and Arabidopsis shoots data.  	  
 * __CompareRoots__ - indicates if there enough data to make a comparison for this term between the rice and Arabidopsis roots data.  	  
 * __AtDEgeneDescr__ - pipe separated list of the unique gene descriptions for the Arabidopsis genes in each group. Duplicated descriptions are listed first. 	  
 * __OsDEgeneDescr__ - pipe separated list of the unique gene descriptions for the rice genes in each group.    

* * *

## AtRoot.genes.txt.gz 

Text file, produced by PortionOfDEGenes.Rmd, giving the genes for each category in the table shown at the end of PortionOfDEGenes.html.
A subset of categories in this file correspond to those shown in AtRootChart.png.     
This file includes the following columns:

 * __gene__ - the name of each gene
 * __category__ - corresponds to one of the rows in the table at the end of PortionOfDEGenes.html.
 * __logFC__ - the log2 fold change of for each gene in the corresponding Arabidopsis roots comparison.
 * __descr__ - gene description

This file (along with its AtShoot, OsRoot, and OsShoot counterparts) is the ideal file to use for pathway or gene ontology comparisons between the sets. Note that terms will need to be seaprated by category after reading the file. I recommend doing this with stack() and unstack().

* * *

## AtRootChart.png

Pie chart produced by PortionOfDEGenes.Rmd, showing the similarities/differences between the rice and Arabidopsis root data sets in terms of the Arabidopsis genes.

* * *

## AtRootChartBlank.png

A duplicate of AtRootChart.png that has no labels so that the labels an be added in power point as needed.

* * *

## AtShoot.genes.txt.gz

Similar to AtRoot.genes.txt.gz.

* * *

## AtShootChart.png

Similar to AtRootChart.png.

* * *

## AtShootChartBlank.png

A duplicate of AtShootChart.png that has no labels so that the labels an be added in power point as needed.

* * *

## OsRoot.genes.txt.gz

Similar to AtRoot.genes.txt.gz.

* * *

## OsRootChart.png

Similar to AtRootChart.png.

* * *

## OsRootChartBlank.png

A duplicate of OsRootChart.png that has no labels so that the labels an be added in power point as needed.

* * *

## OsShoot.genes.txt.gz

Similar to AtRoot.genes.txt.gz.

* * *

## OsShootChart.png

Similar to AtRootChart.png.

* * *

## OsShootChartBlank.png

A duplicate of OsShootChart.png that has no labels so that the labels an be added in power point as needed.
