#!/bin/bash

FS=`ls *.fastq.gz`
O='counts.txt'
for F in $FS
do
    echo -n "$F " >> $O
    gunzip -c $F | grep -c @HISEQ >> $O
done