# Effects of cytokinin on gene expression in rice roots and shoots

This project contains data analysis code and data files for a project investigating how exogenous cytokinin affects gene expression in rice seedlings.

RNA-Seq libraries were prepared from roots and shoots of rice seedlings undergoing a 2 hour treatment with BA (synthetic cytokinin) applied hydroponically. 

To visualize RNA-Seq read alignments, coverage graphs, and junctions inferred from spliced alignments, visit the japonica rice genome in [Integrated Genome Browser](http://bioviz.org/igb).

## About this project 

This project is a collaboration between the Kieber Lab at UNC Chapel
Hill, the Schaller Lab at Dartmouth University, the Solano lab at the National Biotechnology Center, and the [Loraine Lab](http://www.lorainelab.org) at
the North Carolina Research Campus (and UNC Charlotte). A grant from
the US National Science Foundation Plant Genome Research Program to
PI Kieber and Co-PIs Ann Loraine and Eric Schaller funded this work.

* * *

# About this repository

This repository contains analysis folders named for the type of data analysis code and results they contain.

Most analysis folders contain:

* .Rproj file - an RStudio project file. Open this in RStudio to re-run project code.
* .Rmd files - R Markdown files that contain an analysis focused on answering one or two discrete questions. These are typically formatted like miniature research articles, with Introduction, Results/Analysis, and Conclusions sections. 
* .html files - output from running knitr on .Rmd files. 
* results folders - contain files (typically tab-delimited) and images produced by .Rmd files. 
* data - data files obtained from a compute cluster or other external source upstream of this data analysis
* src - R, python, or perl scripts used for simple data processing tasks
* README.md - documentation 

Code used for analysis, making figures, and making data files for
visualization in external site and programs are grouped into folders
with names indicating their common purpose or analysis type. 

Each module folder is designed to be run as a relatively
self-contained project in RStudio. As such, each folder contains an
".Rproj" file. To run the code in RStudio, just open the .Rproj file
and go from there. However, note that the code expects Unix-style file
paths and depends on external libraries, mainly from Bioconductor. 

Some modules depend on the output of other modules. Some modules also
depend on externally supplied data files, which are version-controlled
in ExternalDataSets but may also be available from external sites.

Unless otherwise noted, most code was written by both [Ann Loraine](http://lorainelab.org/people/loraine-bio/) and [Ivory Blakley](http://lorainelab.org/people/about-ivory) in the [Loraine Lab](http://www.lorainelab.org).


* * *

# What's here 

Analysis modules and other directories include .Rmd files (R
Markdown), output from knitting Rmd files in HTML (Web pages), folders
containing results (results) and (sometimes) folders containing
externally supplied data used only in one module (data). Results folders
also contain high-quality image files suitable for inclusion in slides or publications. 

* * *

## Counts 

Makes counts, RPM, and RPKM files. Contains code for 

* processing counts data (RiceCounts.Rmd)
* making RPM and RPKM files (MakeRpmRpkmFiles.Rmd)
* identifying most expressed genes per sample (TopExpressed.Rmd) 

Look in Counts/results for RPM, RPKM, and other counts-related results files.

* * *

## DiffGeneExpr

Identifies differentially expressed genes using tools and libraries
from BioConductor.

Makes files with differential expression testing results:

* S.tsv.gz - compares BA-treated shoots (TS) to mock-treated, control shoots (CS)  
* R.tsv.gz - same as above, but roots (TR vs. CR)
* C.tsv.gz - compares mock-treated, control roots (CR) to mock-treated, control shoots (CS)
* I.tsv.gz - tests for different responsiveness in roots versus shoots to cytokinin (I for "interaction" between tissue type and treatment)

* * *

## ExternalDataSets

Contains rice annotations and other data sets downloaded from IGBQuickLoad.org, GeneOntology.org, and other sources.

* * *

## GeneOntology

Identifies over and under represented GO categories among differentially expressed genes identifed in the DiffGeneExpr module.

Look here for lists of GO categories with unusually many or unusually
few differentially expressed genes.

* * *

## Manuscript

Contains manuscript, figure, and supplemental data files intended for publication.

Note that many of the files in this directory were automatically generated and then edited by hand to improve readability. The script "deployPaper.sh" documents some of this but is out of date. 

* * *

## MapMan analysis

Uses GOSeq to identify MapMan categories containing unusually many or
few cytokinin-regulated genes.

Note: Uses a data file (Osa_MSU_v7.txt.gz) from MapMan Web site. Because of
licensing restrictions, this file is not version-controlled here. To get 
this file, register at the MapMan site and following links labeled 
"download."

* * *

## Motif

Contains code that:

* searches for over-represented motifs and sequences in promoter regions of differentially expressed genes and
* plots the location of motif instances within promoter regions.

by Ivory Blakley

* * * 

## NanoStringsAnalysis

Contains code and data files for analysis of Nanostrings gene expression
analysis.

* * *

## OrthGroups

Uses groups of orthologous genes to compare the cytokinin response in
rice to the response in Arabidopsis.

by Ivory Blakley

* * * 

## PathogenResponseAnalysis

Compares rice RNA-Seq results with results from rice microarray
experiments testing effects of pathogen infection on gene expression.

* * * 

## PbaAnalysis

Contains code and data files used to assess enrichment of OsRR22
binding sites among differentially expressed genes.

* * *

## RootsVsShoots

Compares differential expression in roots to differential expression in shoots.

by Ivory Blakley

* * *

## TranscriptionFactorAnalysis

Investigates cytokinin up and down regulation of transcription factors,
especially transcription factors from the WRKY family.

* * *

## VennDiagram

Generates rectangular, proportionately sized Venn diagram indicating overlap between up- or down-regulated differentially expressed genes in roots and shoots.

by Ivory Blakley

* * *

# Questions?

Contact:

* Ann Loraine aloraine@uncc.edu
* Ivory Blakley ieclabau@uncc.edu

* * *

# License 

Copyright (c) University of North Carolina at Charlotte

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Also, see: http://opensource.org/licenses/MIT