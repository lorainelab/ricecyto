---
title: "Fasta sequences for novel promoter motif search"
author: "Ivory Clabaugh Blakley"
date: "March 17, 2015"
output:
  html_document:
    toc: true
---
**Date run: `r date()`**

# Introduction
In this mark down, we will look for de novo motifs in the promoters of the differentially expressed genes. Motifs that are found to be correlated with a change in expression may lead us to transcription factors that participate the response to cytokinin in rice.

Based on previous studies of promoters, we expect that some form of a TATA box will appear in our results.  Based on previous studies with type-B RRs in both rice and Arabidopsis, we expect that "AGAT" will be part of a type-B binding motif, and is likely to appear in our results.

## Questions

 * What are the sequences of the promoter regions for the genes that were differentially expressed in rice roots or shoots?
 * What motifs are enriched in these sequences?
 
# Method
This mark down has two major parts. First, we will write a fasta file file containing the sequences for the promoter region of each of the differentially expressed genes from this study.  Second, we will look for de novo motifs that are enriched in those sequences using the meme function from the Meme suite.

## Promoter sequences

```{r src, echo=FALSE}
#Load all of the required packages and source code
source("../src/rice_functions.R")
source("MotifMethods.R")
suppressPackageStartupMessages(library("rtracklayer"))
suppressPackageStartupMessages(library("Biostrings"))
```


### Pick out the genes that were differentially expressed in this study.

Read in the results of the differential expression analysis.
```{r}
#Genes detected in roots  #R for root
deR = read.table("../DiffGeneExpr/results/R.tsv.gz", header=T, as.is=T) 
#Genes detected in shoots  #S for shoot
deS = read.table("../DiffGeneExpr/results/S.tsv.gz", header=T, as.is=T) 

# merge the root and shoot data to one data frame
de=merge(deR, deS, by=c("gene","descr"), all=T)
names(de)=gsub(".x", ".R", names(de))
names(de)=gsub(".y", ".S", names(de))
```

Create sets of genes based on their differential expression in one or both parts of the plant.
```{r}
#Get fdr through rice_functions.R
#fdr=getFDR()
#use a strict fdr to limit the sequences to a reasonable number to pass to meme.

#Determine which genes are DE in roots
sigRup = deR[deR$fdr <= 1e-40 & deR$logFC > 2.8, "gene"]
sigRdn = deR[deR$fdr <= 1e-40 & deR$logFC < -2, "gene"]

#Determine which genes are DE in shoots
sigSup = deS[deS$fdr <= 1e-10 & deS$logFC > 1, "gene"]
sigSdn = deS[deS$fdr <= 1e-10 & deS$logFC < -1, "gene"]


#Add the group of genes that were up in both plant parts
sigRSup = de[(de$fdr.R <= 1e-5 & de$logFC.R > 0 & 
             de$fdr.S <= 1e-5 & de$logFC.S > 0), "gene"]

#Add the group that were down in both plant parts
sigRSdn = de[(de$fdr.R <= 1e-5 & de$logFC.R < 0 & 
             de$fdr.S <= 1e-5 & de$logFC.S < 0), "gene"]

#Add the group of genes that were up in roots and down in shoots
sigRupSdn = de[(de$fdr.R <= 1e-3 & de$logFC.R > 0 & 
             de$fdr.S <= 1e-2 & de$logFC.S < 0), "gene"]

#Add the group of genes that were up in shoots and down in roots
sigRdnSup = de[(de$fdr.R <= 1e-3 & de$logFC.R < 0 & 
             de$fdr.S <= 1e-2 & de$logFC.S > 0), "gene"]

#Consider adding genes that are DE in only one tissue, perhaps only one direction.

#Put all groups together in a list. For a group to be included in all the subsequent steps in this mark down, it just needs to be part of this list. The names of the list items will be used to create the file names for fasta files and meme output files. The items themselves should be character vectors giving gene ids.
allSets = list(DEgenesPromotersRup = sigRup,
               DEgenesPromotersRdown = sigRdn,
               DEgenesPromotersSup = sigSup,
               DEgenesPromotersSdown = sigSdn,
               DEgenesPromotersRSup = sigRSup,
               DEgenesPromotersRSdown = sigRSdn,
               DEgenesProm.RupSdown = sigRupSdn,
               DEgenesProm.RdownSup = sigRdnSup)
# The comparisons that use both root and shoot data can produce NA values. Remove those.
allSets = lapply(allSets, na.omit)
# Give a summary of how many genes are in each set.
lapply(allSets, length)
```


### Get the genome sequence
The rice genome is available as a 2bit file, making it efficient for retrieving sequences.
```{r echo=FALSE}
# The 2bit genome file is large, and it is already version controled elsewhere, so it is not part of the repository, and may or may not exist on this machine
# Check to see if it exists, and if it does not, download it.
fname="../ExternalDataSets/O_sativa_japonica_Oct_2011.2bit"
if (!file.exists(fname)){
  print("I couldn't find the rice 2bit genome, so I'm downloading a copy now")
  url="http://www.igbquickload.org/O_sativa_japonica_Oct_2011/O_sativa_japonica_Oct_2011.2bit"
  download.file(url=url, destfile=fname)
}
```
```{r}
Genome <- import.2bit(con=fname)
```



### Promoter regions
Get the promoter region for all of the DE genes.
```{r}
fname='../ExternalDataSets/O_sativa_japonica_Oct_2011.bed.gz'
genes = read.delim(fname,as.is=T,header=F)
genes = genes[c(1:4,6)]
names(genes) = c("chromosome", "chromStart", "chromEnd", "name", "strand")
genes$name = truncateTranscriptID(genes$name)

genes = getGeneRange(genes)[c(2,6,7,1,5)] #function defined in MotifMethods.R
names(genes) = c("chromosome", "chromStart", "chromEnd", "ID", "strand")
genes = genes[!duplicated(genes$ID),]

promSize=500
#only return the promoters for the genes that were DE
deGenes = unique(unlist(allSets))
proms=getPromoterRange(genes, deGenes, size=promSize) #function defined in MotifMethods.R
proms = trimToChromEnds(proms, TwoBitGenome=Genome)

proms = merge(proms, genes[,c("ID", "strand")], by="ID")
```
The promoter size used here was `r promSize`.

### Promoter sequences

Get the sequence for each promoter region. Use the reverse compliment for genes on the negative strand.
```{r}
seqs = subseq(Genome[proms$chromosome], start=proms$promStart+1, end=proms$promEnd) #Seqs being short for sequences
names(seqs)=proms$ID
neg=proms$strand=="-"
seqs[neg] = reverseComplement(seqs[neg])
```

Write the sequences as a fasta file.
```{r echo=FALSE}
# The fasta files are large, and really don't need to be version controled.
# So we will store them in a folder that git will ignore.
system("mkdir intermediateFiles") # in case the folder has not been made yet.
```
```{r}
for (i in 1:length(allSets)){
  fname=paste0("intermediateFiles/", names(allSets)[i], ".fasta")
  writeXStringSet(seqs[allSets[[i]]], fname, format="fasta")
  print(paste0("Created file:  ", fname, ", with ", length(allSets[[i]]), " sequences."))
}
```

## Use meme to look for novel sequences

Run meme with the following parameters:

 * __-maxsize__: The maximum number of bases that meme will accept. This is set based on the total number bases in the sequence file.
 * __-dna__: Use DNA alphabet.
 * __-mod zoops__: Motifs are expected to occur zero or once per sequence. Changing this parameter to 'anr' makes the search process take much longer.
 * __-cons AGAT__: start the analysis using the "AGAT" consensus sequence 
 * __-nmotifs 20__: Meme will stop once it has returned 20 putative motifs.
 * __-minw 6__: Motifs must be at least 6 bases wide.
 * __-maxw 10__: Motifs should be no wider than 10 bases.
 * __-minsites 20__: Only report motifs that occur at least 20 times.
 * __-evt 0.05__: Only report motifs with an Escore of 0.05 or better.
 * __-oc__: Create output directory for each run.
 * note that -revcomp is not in use, only sequences on the given strand are considered.

```{r}
runMeme = function(infile, outfile, ms=60000){
  system(paste("rm -r", outfile)) #remove any old results
  cmd = paste0("meme ", infile, " -maxsize ", ms, " -dna -mod zoops -nmotifs 20 -minw 6 -maxw 10 -minsites 20 -evt 0.05 -oc ", outfile)
  system(cmd)
  # report on these results
  print(paste("See meme results in the", outfile, "folder."))
  reports=length(grep("rc[0123456789]*\\.eps$", dir(outfile)))
  print(paste("The report includes", reports, "possible motifs."))
  html.out=paste0(outfile, "/meme.html")
  return(html.out)
}
```


### Meme results
For each set of genes defined above, run meme (using the runMeme function, defined above), and create a link to the meme output file.

```{r}
#Depending on the parameters to meme and the amount of input sequence, this chunk may several minutes to finish.

comment=NULL
for (name in names(allSets)){
  #setup to run meme
  print(name)
  infile=paste0("intermediateFiles/", name, ".fasta")
  outfile=paste0("results/Meme/MEME.", name)
  ms=promSize*length(allSets[[name]])*1.1
  #runMeme, and save the file name of the html report it creates
  html = runMeme(infile, outfile, ms=ms)
  #add a link to html file
  addComment=paste0("<a href='",html,"'>",html,"</a>")
  comment=c(comment, addComment)
}
comment=paste(comment, collapse="\n\n")
```

Use these links to go directly to the meme html summary.
(Links will only work if the file is present.)

`r comment`

# Conclusions
Using the `r promSize` bp promoter region for genes grouped by direction of change and site of change, there are not many putative de novo motifs.

We do see a TATA motif in several of the comparisons, and it generally occurs within 50 bases of the TSS. There are also motifs that seem to indicate T-rich or A-rich regions, but not necessarily useful motifs. An alternating CG motif was presented a few times, as well as an alternating CT motif.

A few motifs that may be worth follow-up are:   
 * The up-in-roots promoter sequences returned a CATGCATG motif.
 * The down-in-roots promoter sequences returned a C-AGC--G motif.
 * The up-in-both promoter sequences returned a TG{2,4}C{2,4}A motif.
 
The fasta files showing the sequences for each promoter can be used with other software or online tools.  The motifs reported by meme can be viewed and even exported to be evaluated in fimo, or the regular expression can be used in R.


**Session Info and externally defined functions**
```{r}
sessionInfo()
getGeneRange #function defined in MotifMethods.R
getPromoterRange #function defined in MotifMethods.R
system("meme -version", intern=T)
```
The meme suite can be downloaded from http://meme.nbcr.net/meme/doc/download.html     
Use >system("echo $PATH")
to see which directories are part of the path for R/Rstudio.
Install meme in one of these directories.
