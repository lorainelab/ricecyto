---
title: "frequencies of 8-mers"
author: "Ivory Clabaugh Blakley"
date: "April 7, 2015"
output:
  html_document:
    toc: true
---
**Date run: `r date()`**

# Question   
 * Which of the 25 sequences that showed binding for OsRR22 occur in unusually many up or down regulated genes in rice roots or shoots?

# Set up

## Read data 
Read in the hit genes for each sequence.
```{r}
hits = read.delim("results/OsRR22SitesPerGene.txt.gz")
row.names(hits) = hits$gene
```

Read in the differential expression results, and make a set of DE genes.
```{r}
source("../src/rice_functions.R")
de_roots = getDeResultsRoots()
de_shoots = getDeResultsShoots()

fdr=getFDR()
deRup = de_roots$gene[de_roots$fdr<=fdr & de_roots$logFC>0]
deRdown = de_roots$gene[de_roots$fdr<=fdr & de_roots$logFC<0]

deSup = de_shoots$gene[de_shoots$fdr<=fdr & de_shoots$logFC>0]
deSdown = de_shoots$gene[de_shoots$fdr<=fdr & de_shoots$logFC<0]

N=3000
```

## Condense data
For each sequence, get the number of genes that were expressed in each part of the plant (detected with at least one read) and contained the sequence in the promoter region, and get the number of up and down regulated genes that contained the sequence in each part.
```{r}
newRows = c("TotalHits", "RootGeneHits", "UpRegHitsR", "DownRegHitsR", "ShootGeneHits", "UpRegHitsS", "DownRegHitsS")
numHits = data.frame(matrix(nrow=length(newRows), ncol=25))
names(numHits) = names(hits)[4:ncol(hits)]
row.names(numHits) = newRows

numHits["TotalHits",] = apply(hits[4:28], 2, function(x){sum(x>0, na.rm=T)})
# Root
numHits["RootGeneHits",] = apply(hits[de_roots$gene,4:28], 2, function(x){sum(x>0,na.rm=T)})
numHits["UpRegHitsR",] = apply(hits[deRup,4:28], 2, function(x){sum(x>0,na.rm=T)})
numHits["DownRegHitsR",] = apply(hits[deRdown,4:28], 2, function(x){sum(x>0, na.rm=T)})
# Shoot
numHits["ShootGeneHits",] = apply(hits[de_shoots$gene,4:28], 2, function(x){sum(x>0,na.rm=T)})
numHits["UpRegHitsS",] = apply(hits[deSup,4:28], 2, function(x){sum(x>0, na.rm=T)})
numHits["DownRegHitsS",] = apply(hits[deSdown,4:28], 2, function(x){sum(x>0, na.rm=T)})

### The hits only lists genes that do have hits. The de genes lists include genes that don't.
## This is okay, in hits[deSdown,4:28] the genes that dont have hits return na values, 
## and using na.rm=T with sum() means they are not counted and don't interfere with the count.
```

# Analysis

The sequence "AAGATTCG" was found in the promoter regions of `r numHits["TotalHits","AAGATTCG"]` genes.  Of those, only `r numHits["RootGeneHits","AAGATTCG"]` were tested in the root comparison. `r numHits["UpRegHitsR","AAGATTCG"]` of those genes were up regulated and `r numHits["DownRegHitsR","AAGATTCG"]` were down regulated. Is that unusual? To examine this, we can select `r numHits["RootGeneHits","AAGATTCG"]` genes at random from the `r nrow(de_roots)` that were tested, and see how many are DE.  If we repeat this test `r N` times, we can look at the real values in terms of the distribution of comparable random values. We can do this for both roots and shoots. A set of function (shown at the bottom of this document) can be used to iterate this process over each sequence.

```{r echo=FALSE}
# get colors
colors = getColors(c("CR", "CS"))

# make a function to do this
getDistribution = function(expressedGenes, DEgenes, nhits, nDEhits, col, plot=T){
  v=numeric(N)
  for (i in seq(1,N)) {
    v[i]=mean(sample(expressedGenes %in% DEgenes,replace=F,size=nhits))
    }
  v=round(100*v,1)
  realVal=round(100*(nDEhits/nhits),1)
  
  if(plot){
    hs=hist(v, plot=F)
    x0=min(c(hs$mids-.5, realVal-.5))
    x1=max(c(hs$mids+.5, realVal+.5))
    plot(hs, col=col, xlim=c(x0,x1), main="", xlab="% with a site")
    abline(v=realVal, lty=2)
    }
  
  ret=sum(v>realVal)
  return(ret)
  }





plotBothUpDist=function(seq, plot=T){ 
  
  print(seq)
  par(mfcol=c(1,2))
  RootUpDist = getDistribution(expressedGenes=de_roots$gene,
                               DEgenes=deRup,
                               nhits=numHits["RootGeneHits",seq],
                               nDEhits=numHits["UpRegHitsR",seq],
                               col=colors[1], plot=plot)
  #text(x=par('usr')[2], y=par('usr')[4]*1.1, labels=seq, xpd=T, pos=2, cex=2)
  ShootUpDist = getDistribution(expressedGenes=de_shoots$gene,
                                DEgenes=deSup,
                                nhits=numHits["ShootGeneHits",seq],
                                nDEhits=numHits["UpRegHitsS",seq],
                                col=colors[2], plot=plot)
  #text(x=par('usr')[1], y=par('usr')[4]*1.1, labels="% Up-Regulated genes", xpd=T, pos=2, cex=2)
  
  Rup = round(100*(RootUpDist/N),1)
  Sup = round(100*(ShootUpDist/N),1)
  
  if (plot){
    print(paste0("Out of ",N," random sets of ", numHits["RootGeneHits",seq], " genes from the root results, ", RootUpDist," of them (", Rup,"%) had more up regulated genes than the set picked out by having ", seq, " in their promoter regions."))
    print(paste0("Out of ",N," random sets of ", numHits["ShootGeneHits",seq], " genes from the shoot results, ", ShootUpDist," of them (", Sup,"%) had more up regulated genes than the set picked out by having ", seq, " in their promoter regions."))
    }
  
  numHits["RupQuantile",seq] = Rup
  numHits["SupQuantile",seq] = Sup
  
  return(numHits)
  }





plotBothDownDist=function(seq, plot=T){ 
  
  print(seq)
  par(mfcol=c(1,2))
  RootDownDist = getDistribution(expressedGenes=de_roots$gene,
                               DEgenes=deRdown,
                               nhits=numHits["RootGeneHits",seq],
                               nDEhits=numHits["DownRegHitsR",seq],
                               col=colors[1], plot=plot)
  #text(x=par('usr')[2], y=par('usr')[4]*1.1, labels=seq, xpd=T, pos=2, cex=2)
  ShootDownDist = getDistribution(expressedGenes=de_shoots$gene,
                                DEgenes=deSdown,
                                nhits=numHits["ShootGeneHits",seq],
                                nDEhits=numHits["DownRegHitsS",seq],
                                col=colors[2], plot=plot)
  #text(x=par('usr')[1], y=par('usr')[4]*1.1, labels="% Down-Regulated genes", xpd=T, pos=4, cex=2)
  
  Rdown = round(100*(RootDownDist/N),1)
  Sdown = round(100*(ShootDownDist/N),1)
  
  if (plot){
    print(paste0("Out of ",N," random sets of ", numHits["RootGeneHits",seq], " genes from the root results, ", RootDownDist," of them (", Rdown,"%) had more down regulated genes than the set picked out by having ", seq, " in their promoter regions."))
    print(paste0("Out of ",N," random sets of ", numHits["ShootGeneHits",seq], " genes from the shoot results, ", ShootDownDist," of them (", Sdown,"%) had more down regulated genes than the set picked out by having ", seq, " in their promoter regions."))
    }
  
  numHits["RdownQuantile",seq] = Rdown
  numHits["SdownQuantile",seq] = Sdown
  
  return(numHits)
  }
```

## Frequency of UP regulated genes that have each sequence
Compare the number of up regulated genes selected by each sequence to `r N` random sets of the same size. __For the first few, show the plots.__
```{r}
set.seed(45)

for (seq in names(numHits)[1:4]){
  numHits=plotBothUpDist(seq, plot=T)
}
for (seq in names(numHits)[5:ncol(numHits)]){
  numHits=plotBothUpDist(seq, plot=F)
}
```

## Frequency of Down regulated genes that have each sequence
```{r}
for (seq in names(numHits)){
  numHits=plotBothDownDist(seq, plot=F)
}
```


# Results
```{r}
Freqs = data.frame(t(numHits[c("RupQuantile","SupQuantile","RdownQuantile","SdownQuantile"),]))
Freqs = Freqs[order(apply(Freqs, 1, min)),]
towrite = cbind(data.frame(sequence=row.names(Freqs)), Freqs)
write.table(towrite,"results/PerSequenceEnrichment.txt", quote=F, sep="\t", col.names=T, row.names=F)
Freqs
```
Each value in the above table indicates the percentage of randomly selected genes that included as many up (or down) regulated genes in the roots (or shoots) as the set of that had that sequence in their promoters.

For example, "AGATACGC", was in the promoter region of `r numHits["TotalHits","AGATACGC"]` genes, `r numHits["RootGeneHits","AGATACGC"]` of which were tested in the root comparison, and `r numHits["UpRegHitsR","AGATACGC"]` of those were up regulated genes.  If we randomly sampled `r numHits["RootGeneHits","AGATACGC"]` genes from those tested in the roots, we only get that many up regulated genes `r numHits["RupQuantile","AGATACGC"]`% of the time.  The following sequences had values below 5% (ie, the set of genes containing this sequences was better than 95% of random gene sets of similar size at picking out up regulated genes.)
```{r echo=FALSE}
w=which(Freqs$RupQuantile<5)
print(Freqs[w,]["RupQuantile"])
```



# Conclusion
None of the sequences were enriched in down regulated genes in the roots.

```{r echo=FALSE}
Freqs = Freqs[order(Freqs$SdownQuantile),]
w=Freqs$SdownQuantile<5
set = row.names(Freqs[w,])
```
Not many (only `r set`) showed enrichment (top 5%)for down regulated genes in shoots.

```{r echo=FALSE}
Freqs = Freqs[order(Freqs$RupQuantile + Freqs$SupQuantile),]
w=Freqs$RupQuantile<5 & Freqs$SupQuantile<5
set = row.names(Freqs[w,])
```
Some were enriched for up regulated genes in both roots and shoots, like `r set`.

```{r echo=FALSE}
w=Freqs$RupQuantile<5 & Freqs$SupQuantile > 10
setR = row.names(Freqs[w,])
w=Freqs$SupQuantile<5 & Freqs$RupQuantile > 10
setS = row.names(Freqs[w,])
```
While some seem to be more specific to the roots (`r setR`) or the shoots (`r setS`).

Not all of the sequences returned by the protein binding array were preferentially found in regulated genes. The 25 as a group were enriched mainly due to the influence of the sequences above.

```{r echo=FALSE}
w = apply(Freqs, 1, min) > 40
set1 = row.names(Freqs[w,])
w = apply(Freqs, 1, min) > 10
set2 = row.names(Freqs[w,])
set2 = setdiff(set2, set1)
```
Some sequences (`r set1`) did not contribute to the groups enrichment among any category (no values under 40), and several (`r set2`) did not contibute strongly (no values under 10).


## Follow up questions      
 * Was there an overlap in the genes that each of these selected?
 * Where in the promote region did these sequences occur, when considering only the regulated genes that were correlated with each sequence in the above table?
   + Modify OsRR22_seqs1-25.Rmd to answer this or make a new mark down using the its results file: OsRR22_binding_sites.txt.gz
 * Are there other sequences/motifs that are enriched in the same general area of the promoter in the same set of genes?
   + Modify (or make a new version of) PromoterSeqs_deNovoMotifs.Rmd to answer this.

SessionInfo
```{r}
sessionInfo()
getDistribution
plotBothUpDist
plotBothDownDist
```

