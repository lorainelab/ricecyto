category	percent_de	direction	Gene	descr
protein.synthesis.ribosomal protein.eukaryotic.60S subunit.P0	100	1	LOC_Os08g03640	60S acidic ribosomal protein P0, putative, expressed
protein.synthesis.ribosomal protein.eukaryotic.60S subunit.P0	100	1	LOC_Os11g01420	ribosomal protein L10, putative, expressed
protein.synthesis.ribosomal protein.eukaryotic.60S subunit.P0	100	1	LOC_Os11g04070	60S acidic ribosomal protein P0, putative, expressed
protein.synthesis.ribosomal protein.eukaryotic.60S subunit.P0	100	1	LOC_Os12g01430	ribosomal protein L10, putative, expressed
protein.synthesis.ribosomal protein.eukaryotic.60S subunit.P0	100	1	LOC_Os12g03880	60S acidic ribosomal protein P0, putative, expressed
RNA.regulation of transcription.ARR	58.8	1	LOC_Os01g72330	OsRR4  type-A response regulator, expressed
RNA.regulation of transcription.ARR	58.8	1	LOC_Os02g35180	OsRR2  type-A response regulator, expressed
RNA.regulation of transcription.ARR	58.8	1	LOC_Os02g42060	response regulator receiver domain containing protein, expressed
RNA.regulation of transcription.ARR	58.8	1	LOC_Os04g36070	OsRR1  type-A response regulator, expressed
RNA.regulation of transcription.ARR	58.8	1	LOC_Os04g44280	OsRR5  type-A response regulator, expressed
RNA.regulation of transcription.ARR	58.8	1	LOC_Os04g57720	OsRR6  type-A response regulator, expressed
RNA.regulation of transcription.ARR	58.8	1	LOC_Os06g08440	two-component response regulator, putative, expressed
RNA.regulation of transcription.ARR	58.8	1	LOC_Os07g26720	OsRR7  type-A response regulator, expressed
RNA.regulation of transcription.ARR	58.8	1	LOC_Os11g04720	OsRR10  type-A response regulator, expressed
RNA.regulation of transcription.ARR	58.8	1	LOC_Os12g04500	response regulator receiver domain containing protein, expressed
protein.synthesis.elongation	41.5	1	LOC_Os01g71230	nascent polypeptide-associated complex subunit alpha-like protein 3, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os02g12794	elongation factor 1-gamma, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os02g18450	GTP-binding protein typA/bipA, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os02g32030	elongation factor, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os03g02960	nascent polypeptide-associated complex subunit alpha, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os03g08010	elongation factor Tu, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os03g08020	elongation factor Tu, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os03g08050	elongation factor Tu, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os03g29260	elongation factor protein, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os03g63410	elongation factor Tu, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os04g20220	elongation factor Tu, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os04g45490	elongation factor, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os05g31000	nascent polypeptide-associated complex subunit alpha, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os06g37440	elongation factor 1-gamma, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os07g42300	elongation factor protein, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os07g46750	elongation factor protein, putative, expressed
protein.synthesis.elongation	41.5	1	LOC_Os11g02450	elongation factor P, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os01g03070	transposon protein, putative, unclassified, expressed
protein.synthesis.initiation	27.1	1	LOC_Os02g54700	RNA recognition motif containing protein, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os03g08450	eukaryotic translation initiation factor 3 subunit K, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os03g18510	expressed protein
protein.synthesis.initiation	27.1	1	LOC_Os03g21550	eukaryotic translation initiation factor 2 subunit beta, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os04g30780	eukaryotic translation initiation factor 3 subunit H, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os04g42140	eukaryotic initiation factor iso-4F subunit p82-34, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os05g16660	WD domain, G-beta repeat domain containing protein, expressed
protein.synthesis.initiation	27.1	1	LOC_Os05g49150	eukaryotic translation initiation factor 3 subunit D, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os06g48350	CPuORF14 - conserved peptide uORF-containing transcript, expressed
protein.synthesis.initiation	27.1	1	LOC_Os06g48355	expressed protein
protein.synthesis.initiation	27.1	1	LOC_Os07g03230	eukaryotic translation initiation factor 3 subunit C, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os07g12110	eukaryotic translation initiation factor 3 subunit E, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os07g44620	eukaryotic translation initiation factor 6, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os08g21660	WD domain, G-beta repeat domain containing protein, expressed
protein.synthesis.initiation	27.1	1	LOC_Os09g34010	expressed protein
protein.synthesis.initiation	27.1	1	LOC_Os10g41960	eukaryotic translation initiation factor 3 subunit B, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os12g07740	eukaryotic translation initiation factor 2 subunit gamma, putative, expressed
protein.synthesis.initiation	27.1	1	LOC_Os12g41400	eukaryotic translation initiation factor 2 subunit gamma, putative, expressed
protein.folding	25.3	1	LOC_Os02g22780	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os02g39870	co-chaperone GrpE protein, putative, expressed
protein.folding	25.3	1	LOC_Os03g04970	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os03g25050	chaperonin, putative, expressed
protein.folding	25.3	1	LOC_Os03g42220	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os03g59020	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os04g36890	peptidyl-prolyl cis-trans isomerase, FKBP-type, putative, expressed
protein.folding	25.3	1	LOC_Os04g46620	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os05g05470	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os05g46290	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os05g48290	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os06g02380	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os06g36700	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os07g44740	chaperonin, putative, expressed
protein.folding	25.3	1	LOC_Os08g25090	co-chaperone GrpE protein, putative, expressed
protein.folding	25.3	1	LOC_Os09g12270	peptidyl-prolyl cis-trans isomerase, FKBP-type, putative, expressed
protein.folding	25.3	1	LOC_Os09g38980	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os10g32550	T-complex protein, putative, expressed
protein.folding	25.3	1	LOC_Os12g17910	T-complex protein, putative, expressed
