MapMan Analysis of Cytokinin-regulated genes in rice roots and shoots
========================================================

This R markdown document describes using Bioconductor package called goseq to determine MapMan categories that contain unusually many or few differentially cytokinin-regulated genes.

Questions:

* What types of genes are unusually abundant among genes that are up- or down-regulated in BA-treated roots or shoots?

Analysis
---------

Differential gene expression results have size bias. To account for this, we'll use the GOSeq package.

### Read annotations and transcript sizes

Read transcript sizes:

```{r}
fname='../ExternalDataSets/tx_size.txt.gz'
sizes=read.delim(fname,header=T,sep='\t')
```


Read MapMan annotations, clean up the data:

```{r}
source("src/MapMan.R")
gene2cat.orig=getMapManMSU7riceCategories()
gene2cat=gene2cat.orig[,c('NAME','IDENTIFIER')]
names(gene2cat)=c('category','Gene')
v=which(!gene2cat$Gene=='')
gene2cat=gene2cat[v,]
v=paste(gene2cat$category,gene2cat$Gene)
v=!duplicated(v)
gene2cat=gene2cat[v,]
```
```{r echo=FALSE}
saveModifiedMapManCategories(gene2cat.orig)
```

Read project-wide setting for FDR cutoff:

```{r}
source('../src/rice_functions.R')
de_FDR=getFDR()
cat_FDR=de_FDR*10
```

Threshold for selecting DE genes is:

* FDR `r de_FDR`

Threshold for selecting signficant GO categories is:

* FDR `r cat_FDR`

Read transcript sizes:

```{r}
sizes=getTranscriptSizes()
```

Load goseq library:

```{r}
suppressPackageStartupMessages(library(goseq))
```

Function for analyzing category enrichment. Arguments:

* de_FDR - FDR cutoff for differential expression
* cat_FDR - FDR cutoff for category significance
* d - DE results 
* sizes - vector with transcript sizes in kb, output of getTranscriptSizes() from src/rice_functions.R
* gene2cat - same as gene2cat above, genes mapped to GO terms

```{r}
do.cat=function(de_FDR=NULL,d=NULL,
               sizes=NULL,
               gene2cat=NULL,fname=NULL,
               cat_FDR=NULL){
  gene.vector=rep(0,nrow(d))
  names(gene.vector)=row.names(d)
  length.vector=sizes[names(gene.vector)]
  v=d$fdr<=de_FDR
  gene.vector[v]=1
  pwf<-nullp(gene.vector,bias.data=length.vector,plot.fit=T)
  Cat=goseq(pwf,gene2cat=gene2cat,method="Wallenius")
  Cat$over.fdr=p.adjust(Cat$over_represented_pvalue,method='BH')
  Cat$under.fdr=p.adjust(Cat$under_represented_pvalue,method='BH')
  Cat=Cat[union(which(Cat$over.fdr<=cat_FDR),
              which(Cat$under.fdr<=cat_FDR)),]
  o=order(Cat$over.fdr,decreasing=F)
  Cat=Cat[o,c('category','numDEInCat',
            'numInCat','over.fdr','under.fdr')]
  if (!is.null(fname)) {
    write.table(Cat,file=fname,row.names=F,quote=F,sep="\t")
  }
  Cat
}
```

### DE genes in roots

Read differential expression results:

```{r}
d=getDeResultsRoots()
```

Get enriched and depleted MapMan categories:

```{r fig.width=4,fig.height=4}
Cat=do.cat(de_FDR=de_FDR,d=d,sizes=sizes,gene2cat=gene2cat,cat_FDR=cat_FDR)
fname='results/MapManFit-Roots.png'
quartz.save(fname,type='png',height=4,width=4)
```


How many genes with MapMan annotations are DE?

```{r}
denom=length(intersect(unique(gene2cat$Gene),d$gene))
numer=length(intersect(unique(gene2cat$Gene),d[d$fdr<=de_FDR,'gene']))
total_percent=round(numer/denom*100,1)
```

Recall that we did not test every gene for differential expression - only the genes where we observed at least one read. We observed at least one read for `r nrow(d)` genes. Of these, there were `r denom` genes with at least one MapMan annotation. Of the genes that we tested for differential expression and had a MapMan annotation, there were `r numer` that were differentially expressed with FDR `r de_FDR` or less. Thus, `r total_percent`% of MapMan-annotated, expressed genes were also differentially expressed in treated versus untreated roots.

If all DE genes are distributed evenly to all MapMan categories, keeping in mind that a gene can be annotated to multiple categories, then we expect that on average, categories will contain around `r total_percent`% DE genes. 

Examine distribution of percentages among the enriched and depleted categories:

```{r fig.height=3,fig.width=3}
percent_de=round(Cat$numDEInCat/Cat$numInCat*100,1)
Cat$percent_de=percent_de
h=hist(Cat$percent_de,plot=F)
top=max(h$counts)+2
plot(h,main="MapMan categories",xlab="percent DE in roots",label=T,col="lightblue",ylim=c(0,top),xlim=c(0,100))
abline(v=total_percent,col="magenta")
fname='results/MapManHistRoots.png'
quartz.save(fname,dpi=300,width=3,height=3,type="png")
```

Categories with an unusually large percentage of DE genes: `r Cat[Cat$over.fdr<=cat_FDR,]$category`.

Categories with an unusually small percentage of DE genes: `r Cat[Cat$under.fdr<=cat_FDR,]$category`.

#### Examine DE genes in over-represented categories

Are genes in over-represented categories mostly up or down regulated?

```{r}
de=d[d$fdr<=de_FDR,]$gene
CatplusGenes=merge(Cat[Cat$over.fdr<=cat_FDR,],
                  gene2cat[gene2cat$Gene%in%de,],
                  by.x='category',by.y='category')
CatplusGenes=merge(CatplusGenes,
                  d[,c('gene','logFC','descr')],
                  by.x='Gene',by.y='gene')
o=order(CatplusGenes$percent_de,CatplusGenes$category,decreasing=T)
v=rep(0,nrow(CatplusGenes))
v[CatplusGenes$logFC>0]=1
v[CatplusGenes$logFC<0]=-1
CatplusGenes$direction=v
CatplusGenes=CatplusGenes[o,c('category','percent_de','direction',
                            'Gene','descr')]
fname='results/MapManPlusGenes-R.txt'
write.table(CatplusGenes,file=fname,
            row.names=F,quote=F,sep='\t')
```

What is percentage of DE genes that were up-regulated?

```{r}
Cat$numUPInCat=rep(NA,nrow(Cat))
Cat$percent_up=rep(NA,nrow(Cat))
v=which(Cat$over.fdr<=cat_FDR)
for (i in v) {
  category=Cat[i,'category']
  up=sum(CatplusGenes[CatplusGenes$category==category,]$direction==1)
  Cat[i,'numUpInCat']=up
  Cat[i,'percent_up']=round(up/Cat[i,'numDEInCat']*100,1)
}
Cat=Cat[,c('category','numDEInCat',
         'numUpInCat','numInCat',
         'percent_up','percent_de',
         'over.fdr','under.fdr')]
```

Write a file:

```{r}
fname='results/MapMan-R.txt'
Cat=Cat[order(Cat$percent_de,decreasing=T),]
write.table(Cat,file=fname,row.names=F,quote=F,sep="\t")
```

  
This analysis created a tab-separated file named `r fname` and identified `r length(which(Cat$over.fdr<=cat_FDR))` enriched MapMan categories and `r length(which(Cat$under.fdr<=cat_FDR))` under-represented MapMan categories using category FDR `r cat_FDR`.

### DE genes in shoots

Read differential expression results and merge with transcript sizes:

```{r}
d=getDeResultsShoots()
```


Get enriched and depleted MapMan categories:

```{r fig.height=4,fig.width=4}
Cat=do.cat(de_FDR=de_FDR,d=d,sizes=sizes,gene2cat=gene2cat,cat_FDR=cat_FDR)
fname='results/MapManFit-Shoots.png'
quartz.save(fname,type='png',height=4,width=4)
```


How many genes with MapMan term annotations are DE?

```{r}
denom=length(intersect(unique(gene2cat$Gene),d$gene))
numer=length(intersect(unique(gene2cat$Gene),d[d$fdr<=de_FDR,'gene']))
total_percent=round(numer/denom*100,1)
```

Recall that we did not test every gene for differential expression - only the genes where we observed at least one read. We observed at least one read for `r nrow(d)` genes. Of these, there were `r denom` genes with at least one annotation. Of the genes that we tested for differential expression and had an annotation, there were `r numer` that were differentially expressed with FDR `r de_FDR` or less. Thus, `r total_percent`% of MapMan-annotated, expressed genes were also differentially expressed in treated versus untreated shoots.

If all DE genes are distributed evenly to all MapMan categories, keeping in mind that a gene can be annotated to multiple categories, then we expect that on average, categories will contain around `r total_percent`% DE genes. 

Examine distribution of percentages among the enriched and depleted categories:

```{r fig.height=3,fig.width=3}
percent_de=round(Cat$numDEInCat/Cat$numInCat*100,1)
Cat$percent_de=percent_de
h=hist(Cat$percent_de,plot=F)
top=max(h$counts)+2
plot(h,main="MapMan categories",xlab="percent DE in shoots",label=T,col="lightblue",ylim=c(0,top),
     xlim=c(0,100))
abline(v=total_percent,col="magenta")
fname='results/MapManHistShoots.png'
quartz.save(fname,dpi=300,width=3,height=3,type="png")
cat_FDR=cat_FDR*100
```

Note: We'll use a larger FDR for assessing enriched categoryies: `r de_FDR`.
Categories with an unusually large percentage of DE genes: `r Cat[Cat$over.fdr<=cat_FDR,]$category`.

Categories with an unusually small percentage of DE genes: `r Cat[Cat$under.fdr<=cat_FDR,]$category`.

#### Examine DE genes in over-represented categories

Are genes in over-represented categories mostly up or down regulated?

```{r}
de=d[d$fdr<=de_FDR,]$gene
CatplusGenes=merge(Cat[Cat$over.fdr<=cat_FDR,],
                  gene2cat[gene2cat$Gene%in%de,],
                  by.x='category',by.y='category')
CatplusGenes=merge(CatplusGenes,
                  d[,c('gene','logFC','descr')],
                  by.x='Gene',by.y='gene')
o=order(CatplusGenes$percent_de,CatplusGenes$category,decreasing=T)
v=rep(0,nrow(CatplusGenes))
v[CatplusGenes$logFC>0]=1
v[CatplusGenes$logFC<0]=-1
CatplusGenes$direction=v
CatplusGenes=CatplusGenes[o,c('category','percent_de','direction',
                            'Gene','descr')]
fname='results/MapManPlusGenes-S.txt'
write.table(CatplusGenes,file=fname,
            row.names=F,quote=F,sep='\t')
```

What is percentage of DE genes that were up-regulated?

```{r}
Cat$numUPInCat=rep(NA,nrow(Cat))
Cat$percent_up=rep(NA,nrow(Cat))
v=which(Cat$over.fdr<=cat_FDR)
for (i in v) {
  category=Cat[i,'category']
  up=sum(CatplusGenes[CatplusGenes$category==category,]$direction==1)
  Cat[i,'numUpInCat']=up
  Cat[i,'percent_up']=round(up/Cat[i,'numDEInCat']*100,1)
}
Cat=Cat[,c('category','numDEInCat',
         'numUpInCat','numInCat',
         'percent_up','percent_de',
         'over.fdr','under.fdr')]
```

Write a file:

```{r}
fname='results/MapMan-S.txt'
Cat=Cat[order(Cat$percent_de,decreasing=T),]
write.table(Cat,file=fname,row.names=F,quote=F,sep="\t")
```

  
This analysis created a tab-separated file named `r fname` and identified `r length(which(Cat$over.fdr<=cat_FDR))` enriched MapMan categories and `r length(which(Cat$under.fdr<=cat_FDR))` under-represented MapMan categories using MapMan term FDR `r cat_FDR`.

Conclusions
----------

* In both roots and shoots, categories related to cytokinin signaling were enriched with DE genes.
* In roots, unusually many genes annotated to MapMan category "biotic stress" were differentially express.
* In shoots, categories related to protein synthesis were enriched with DE genes. 

Session information
-------------------

```{r}
sessionInfo()
getMapManMSU7riceCategories
saveModifiedMapManCategories
```




