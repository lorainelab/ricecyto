#!/bin/bash

# shell script that copies stuff from repo into 
# DropBox folder for all the authors to read, review

# removes all old stuff first
D=~/Dropbox/RiceRNASeqPaper
rm $D/*

# copy the manuscript
# cp RiceRNASeq.docx $D/.

# copy and rename the table(s)
cp NanostringsTable.docx $D/TableI.docx

# copy and rename the figures
cp BasicRvS_Overview.pptx $D/Figure1.pptx
cp NanoStringsPlots.pptx $D/Figure2.pptx
cp PBAresults.pptx $D/Figure3.pptx
cp AtVsRiceOrtholgy.pptx $D/Figure4.pptx
cp MapManRiceVsAt.pptx $D/Figure5.pptx
cp MapManVisuals.pptx $D/Figure6.pptx
cp PathogenVenn.pptx $D/Figure7.pptx
cp WRKYgenesBarPlot.pptx $D/Figure8.pptx


# copy the combined supplemental figues
cp SupplementalFigures.pptx $D/.
# SF1 - Baplots of genes involved in CK biosynthesis, signaling, degredation
# SF2 - binding of OsRR27 and OsRR29
# SF3 - GO - genes induced by cytokinin in both the root and shoot
# SF4 - GO - genes repressed by cytokinin in both the root and shoot
# SF5 - GO - genes induced by cytokinin in the root 
# SF6 - GO - genes induced by cytokinin in the shoot 
# SF7 - GO - genes repressed by cytokinin in the shoot 
# SF8 - MapMan rice and At shoots

# copy and rename the supplemental tables
S=SupplementalDataFileS
X='xls' # easier for most readers' default settings
# gunzip -c ../Counts/results/rice.tsv.gz > $D/$S-1.$X
cp SupplementalTableS-1.xlsx $D/$S-1.xlsx
# gunzip -c ../Counts/results/rice_RPKM.tsv.gz > $D/$S-2.$X
cp SupplementalTableS-2.xlsx $D/$S-2.xlsx
# gunzip -c ../DiffGeneExpr/results/C.tsv.gz > $D/$S-3.$X
cp SupplementalTableS-3.xlsx $D/$S-3.xlsx
# gunzip -c ../DiffGeneExpr/results/R.tsv.gz > $D/$S-4.$X
cp SupplementalTableS-4_entire.xlsx $D/$S-4.xlsx
# gunzip -c ../DiffGeneExpr/results/S.tsv.gz > $D/$S-5.$X
cp SupplementalTableS-5.xlsx $D/$S-5.xlsx
# cut -f1-8 ../DiffGeneExpr/results/DESameDirection.txt > $D/$S-6.$X
cp SupplementalTableS-6.xlsx $D/$S-6.xlsx
# cut -f1-8 ../DiffGeneExpr/results/DEOppositeFoldChange.txt > $D/$S-7.$X
cp SupplementalTableS-7.xlsx $D/$S-7.xlsx
# cp ../MapManAnalysis/results/MapManPlusGenes-R.txt $D/$S-8.$X
cp SupplementalTableS-8.xlsx $D/$S-8.xlsx
# cp ../MapManAnalysis/results/MapManPlusGenes-S.txt $D/$S-9.$X
cp SupplementalTableS-9.xlsx $D/$S-9.xlsx
