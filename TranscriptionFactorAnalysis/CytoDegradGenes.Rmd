Analysis of cytokinin degradation genes - CKXs
========================================================

Questions:

* Were many genes of the OsCKXs regulated in BA-treated roots or shoots?


Analysis
--------

Setting up:

```{r}
source('../src/rice_functions.R')
de_FDR=getFDR()
CKXs = getCKXs()
```

There are `r length(CKXs)` OsCKXs in rice.

Read roots and shoots differential expression results:

```{r}
roots=getDeResultsRoots()
roots=roots[roots$fdr<=de_FDR,]
shoots=getDeResultsShoots()
shoots=shoots[shoots$fdr<=de_FDR,]
```

How many OsCKXs were differentially expressed?

```{r}
de_roots_fam=roots[which(roots$gene%in%CKXs),]
de_shoots_fam=shoots[which(shoots$gene%in%CKXs),]
```

Out of `r length(CKXs)` OsCKXs, `r nrow(de_roots_fam)` were differentially expressed in BA-treated rice roots. And of these, `r sum(de_roots_fam$logFC>0)` were up regulated.

Out of `r length(CKXs)` OsCKXs, `r nrow(de_shoots_fam)` were differentially expressed in BA-treated rice shoots. And of these, `r sum(de_shoots_fam$logFC>0)` were up regulated.

Bar chart
-----------
```{r echo=FALSE}
# Set up variables for plot.
bars = merge(data.frame(gene = de_roots_fam$gene,
                        logFC = de_roots_fam$logFC),
                        #part = rep("root", nrow(de_roots_fam)),
                        #descr = de_roots_fam$descr),
             data.frame(gene = de_shoots_fam$gene,
                        logFC = de_shoots_fam$logFC),
                        #part = rep("shoot", nrow(de_shoots_fam)),
                        #descr = de_shoots_fam$descr), 
                        by="gene", all=T)
# typeA gene names
CKXnames = names(CKXs)
names(CKXnames) = CKXs
bars$name = CKXnames[as.character(bars$gene)]

ord = order(as.numeric(gsub("OsCKX","", bars$name))) #order by gene name
bars = bars[ord,]

# colors
colors = getColors(c("CR","CS"))
names(colors) = c("root", "shoot")
borders = c(NA, "black")
names(borders) = c("root", "shoot")
```


```{r echo=FALSE, fig.width=9, fig.height=4}
# Make plot.
# put all the code into a function so you can call it for the mark down and call it to save to a file without duplicating code.

makeThisPlot=function(){
ylab=expression(paste("log"[2],"FC"))
main="Cytokinin regulation of CKXs in rice"

bp=barplot(t(as.matrix(bars[,2:3])), names.arg=rep("", nrow(bars)), beside=T,
           las=2, col=colors, border=borders[bars$part], axes=F, 
           ylab="", main=main)
axis(side=2, at=0:8, labels=0:8, las=2, xpd=T)
abline(h=0, lwd=1.5)

# add labels --- if you want them tilted
at=apply(cbind(bars[,2:3],0), 1, min, na.rm=T)*1.4 - .2
text(x=bp[2,], y=at, labels=bars$name, adj=1, srt=45, xpd=T)

#add y-axis label
mtext(text=ylab, side=2, line=1.8)

#add x's where gene is DE in only one tissue
bp2 = bp
bp2[1,][!is.na(bars$logFC.x)] = NA
bp2[2,][!is.na(bars$logFC.y)] = NA
text(x=bp2, y=0, labels="X", adj=c(.5, -.1), cex=1, col=gray(.3))

# add legend
legend(x="topright", legend=names(colors), bty="n", fill=colors, border=borders[names(colors)])
}
makeThisPlot()
#save
fname="results/CKXbars.png"
png(fname, res=600, width=4.5, height=5, units="in")
makeThisPlot()
dev.off()
```



Conclusions
-----------



Session Information
---------------------
Show session info and display functions that were defined eleswhere.
```{r}
sessionInfo()
getCKXs
getDeResultsRoots
getDeResultsShoots
makeThisPlot
```