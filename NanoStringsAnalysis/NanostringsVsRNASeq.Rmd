Comparing RNA-Seq and Nanostrings fold-changes
==============================================

Introduction
-------------

We identified many thousands of differentially expressed genes in rice seedlings undergoing treatment with exogenous cytokinin, mostly in roots. 

We then re-tested a subset of these genes using samples from a second run of the same experiment, but using the nCounter Nanostrings system to measure gene expression. 

We expect that the log2 fold-changes obtained from the DE genes will be similar, but not identical.

Question:

 * How similar were RNA-Seq and Nanostring measurements?

The Nanostrings results are stored in two files:

* data/Roots.xlsm
* data/Shoots.xlsm

The same genes were tested in three biological replicates of BA- and mock-treated roots and shoots.

Analysis
---------

## Reading Nanostrings data 

```{r}
source("../src/rice_functions.R")
suppressPackageStartupMessages(library("xlsx"))
fname="data/Roots.xlsm"
header=read.xlsx(fname, sheetIndex=1, colIndex=4:15, rowIndex=5, stringsAsFactors=F, header=F)
nsR=read.xlsx(fname, sheetIndex=1, startRow=22, stringsAsFactors=F)
names(nsR)[4:15] = header
keep=c(1:3, grep("NIP", names(nsR)))
nsR = nsR[,keep]
fname="data/Shoots.xlsm"
#The spreadsheet comes in sections, get the sample names from the top section.
header=read.xlsx(fname, sheetIndex=1, colIndex=4:15, rowIndex=5, stringsAsFactors=F, header=F)
nsS=read.xlsx(fname, sheetIndex=1, startRow=22, stringsAsFactors=F)
names(nsS)[4:15] = header
#only keep the samples that relate to this study
keep=c(1:3, grep("NIP", names(nsS)))
nsS = nsS[,keep]
```

Note that the Nanostrings spreadsheets contained sample names in the top rows and data in the lower rows. The spreadsheets also contain data not related to this study - we'll ignore that.

Merge the two sets into one data frame and only keep data for genes. These are noted as "Endogenous" and "Housekeeping". 

```{r}
ns = merge(nsR, nsS, by=names(nsR)[1:3])
ns = ns[grep('Endogenous|Housekeeping', ns$Code.Class),]
row.names(ns) = ns$Name
```

Note: We are not normalizing here; it might be a good idea to normalize the Nanostrings expression data using standards and/or housekeeping genes that were included in the experiment. For now, we won't do this.

The Nanostrings experiment tested `r length(unique(ns$Name))` genes.

## Testing for differential expression 

Group samples by treatment (BA or control) or plant part (root or shoot):

```{r}
CR = grep("R-CTRL", names(ns))
TR = grep("R-BA", names(ns))
CS = grep("S-CTRL", names(ns))
TS = grep("S-BA", names(ns))
```

Calculate log2 fold-change, and while we're at it, test for differential expression using a t test:

```{r}
ns$p.val.R = NA
ns$p.val.S = NA
ns$logFC.R = NA
ns$logFC.S = NA

for (gene in row.names(ns)){
  #root comparison
  ttR=t.test(x=ns[gene, CR], y=ns[gene, TR])
  ns[gene, "p.val.R"] = ttR$p.value
  ns[gene, "logFC.R"] = log2(mean(unlist(ns[gene, TR]))/mean(unlist(ns[gene, CR])))
  #shoot comparison
  ttS=t.test(x=ns[gene, CS], y=ns[gene, TS])
  ns[gene, "p.val.S"] = ttS$p.value
  ns[gene, "logFC.S"] = log2(mean(unlist(ns[gene, TS]))/mean(unlist(ns[gene, CS])))
}
nano_pval=0.1
num_de_roots=sum(ns$p.val.R<=nano_pval)
num_de_shoots=sum(ns$p.val.S<=nano_pval)
```

Read RNA-Seq results:

```{r}
seqR=getDeResultsRoots()
seqS=getDeResultsShoots()
```

Extract results for genes that were tested using Nanostrings:

```{r}
de_FDR=getFDR()
tested=ns$Name
seqR=seqR[tested,]
seqR=seqR[seqR$fdr<=de_FDR,]
seqS=seqS[tested,]
seqS=seqS[seqS$fdr<=de_FDR,]
```

## Compare fold-change from roots

```{r fig.width=3, fig.height=3}
par(cex=1, mgp=c(c(2, .7, 0)), las=1)
main=expression(paste("Log"[2],"FC in roots"))
xlab="RNA-Seq"
ylab="Nanostrings"
tested=row.names(seqR)
nsR=ns[tested,]
plot(nsR$logFC.R~seqR$logFC,
     main="",xlab=xlab,ylab=ylab,pch=16)
title(main, line=1)
model=lm(nsR$logFC.R~seqR$logFC)
abline(model, col="red")
fname='results/NanoVRNASeqRootsLog2FC.png'
quartz.save(fname,type='png',dpi=600,width=4,height=4)
```

Of `r nrow(nsR)` genes tested by Nanostrings that were DE in the roots BA- versus mock-treated comparison, `r nrow(nsR[nsR$p.val.R<=nano_pval,])` were DE according to the t test applied to the Nanostrings expression values.

Pearson's correlation coefficient between Nanostrings and RNA-Seq log2 fold-change was `r round(cor(nsR$logFC.R,seqR$logFC),3)` and r-squared of a model fit to the data was `r round(summary(model)$r.squared,2)`.

Out of `r nrow(nsR)` genes tested by Nanostrings that were DE by RNA-Seq, `r sum(nsR$logFC.R*seqR$logFC<0)` changed in the opposite direction.

The log2 fold-change obtained from Nanostrings is very similar to log2 fold-change obtained from RNA-Seq.

## Compare fold-change from shoots

```{r fig.width=3, fig.height=3}
par(cex=1, mgp=c(c(2.4, .7, 0)), las=1)
main=expression(paste("Log"[2],"FC in shoots"))
xlab="RNA-Seq"
ylab="Nanostrings"
tested=row.names(seqS)
nsS=ns[tested,]
plot(nsS$logFC.S~seqS$logFC,
     main="",xlab=xlab,ylab=ylab,pch=16)
title(main, line=1)
model=lm(nsS$logFC.S~seqS$logFC)
abline(model, col="red")
fname='results/NanoVRNASeqShootsLog2FC.png'
quartz.save(fname,type='png',dpi=600,width=4,height=4)
```

Of `r nrow(nsS)` genes tested by Nanostrings that were DE in the shoots BA- versus mock-treated comparison, `r nrow(nsS[nsS$p.val.S<=nano_pval,])` were DE according to a t test of the Nanostrings data.

Pearson's correlation coefficient between Nanostrings and RNA-Seq log2 fold-change wasand `r round(cor(nsS$logFC.S,seqS$logFC),3)` and r-squared of a model fit to the data was `r round(summary(model)$r.squared,2)`.

Out of `r nrow(nsS)` genes tested by Nanostrings that were DE by RNA-Seq, `r sum(nsS$logFC.S*seqS$logFC<0)` changed in the opposite direction.

## Summary Table
Write out a table with these results.
```{r echo=FALSE}

# assemble columns
nanoTable = merge(seqR[c("logFC", "fdr")], ns[c("logFC.R", "p.val.R")], by="row.names", all.y=T)
nanoTable = merge(nanoTable, seqS[c("logFC", "fdr")], by.x="Row.names", by.y="row.names", all.x=T)
nanoTable = merge(nanoTable, ns[c("logFC.S", "p.val.S")], by.x="Row.names", by.y="row.names", all=T)

# replace non-sig values with a '-'
nonSigRoot = which(nanoTable$fdr.x > de_FDR | is.na(nanoTable$fdr.x))
nanoTable$logFC.x[nonSigRoot] = NA
nanoTable$logFC.R[nonSigRoot] = NA
nonSigShoot = which(nanoTable$fdr.y > de_FDR | is.na(nanoTable$fdr.y))
nanoTable$logFC.y[nonSigShoot] = NA
nanoTable$logFC.S[nonSigShoot] = NA

# remove genes that were not DE in rna-seq roots or shoots.
w = which(!(nanoTable$fdr.x > de_FDR & nanoTable$fdr.y > de_FDR))
nanoTable = nanoTable[w,]

# add gene descriptions
descr = read.delim("data/DescriptionsForRetestedGenes.txt")
nanoTable = merge(descr, nanoTable, by.x="gene", by.y="Row.names")

# format table
nanoTable = nanoTable[c(1:3,5,7,9)]
names(nanoTable) = c("gene", "description", "Root-RNA-Seq", "Root-Nanostring", "Shoot-RNA-Seq", "Shoot-Nanostring")
nanoTable[3:6] = sapply(nanoTable[3:6], round, digits=2)

# write table
fname='results/nanoTable.txt'
write.table(nanoTable, file=fname, na="-", quote=F, sep='\t', row.names=F, col.names=T)
```
A table of the RNA-Seq and nanostring fold changes for these genes has been saved as `r fname`.

Conclusions
-----------

* Fold-changes obtained from Nanostrings and RNA-Seq were close.
* The Nanostrings tended to validate the RNA-Seq results.

Session Info
-------------

```{r}
sessionInfo()
```